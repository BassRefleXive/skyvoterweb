var path = require('path');
var webpack = require('webpack');
var node_modules_dir = path.join(__dirname, 'node_modules');

const babelOptions = {
    "presets": ["es2015", "react", "stage-0"],
    "plugins": [
        "transform-inline-environment-variables",
        "transform-react-require",
        "react-require",
    ],
};

const babelConfig = JSON.parse(JSON.stringify(babelOptions))

babelConfig.cacheDirectory = "tmp-build/webpackCache"


var config = {
    name: "app",
    devtool: "source-map",
    context: __dirname,
    node: {
        __filename: true
    },
    entry: {
        app: [path.join(__dirname, "src/app.js")],
    },
    output: {
        path: path.join(__dirname, 'web/dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    resolve: {
        extensions: ["", ".js", ".jsx", ".json"],
        root: path.join(__dirname, '/src')
    },
    module: {
        preLoaders: [
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: `babel-loader?${JSON.stringify(babelConfig)}`,
            },
            {
                test: /\.json?$/,
                loader: 'json-loader',
            },
            { test: /(\.css$)/, loaders: ['style-loader', 'css-loader'] }, { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
        ],
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin(
            "vendor",
            "app.vendor.js",
            function (module) {
                return module.resource && module.resource.indexOf(path.join(__dirname, 'src')) === -1;
            }
        ),
    ],
    externals: {
    }
};


module.exports = config;