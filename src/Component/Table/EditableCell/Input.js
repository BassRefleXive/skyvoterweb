import { Input as AntInput } from 'antd';

const Input = ({ editable, value, onChange }) => (
    <div>
        {editable
            ? <AntInput style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
            : value
        }
    </div>
);

export default Input;