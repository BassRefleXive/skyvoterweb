import {Select as AntSelect} from 'antd';

const Select = ({editable, value, onChange, options}) => {
    return (
        <div>
            {editable
                ? (
                    <AntSelect {...{
                        onChange: value => onChange(value),
                        style: {
                            width: 150,
                        },
                        defaultValue: options.filter(({text}) => {
                            return text === value;
                        })[0].value
                    }}>
                        {options.map(({value, text}) => <AntSelect.Option value={value} key={value}>{text}</AntSelect.Option>)}
                    </AntSelect>
                )
                : value
            }
        </div>
    );
};

export default Select;
