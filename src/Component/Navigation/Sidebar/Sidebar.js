import React from 'react';
import {connect} from 'react-redux';
import {Layout} from 'antd';

import Client from './Component/Client'
import Admin from './Component/Admin'

const mapState = ({
                      user: {
                          user,
                      },
                  }) => {
    return {
        user
    }
};

class Sidebar extends React.Component {
    render() {
        const {
            user,
        } = this.props;

        return (
            <Layout.Sider width={200} style={{background: '#fff'}}>
                {user.isAdmin() ? <Admin/> : <Client/>}
            </Layout.Sider>
        );
    }
}

export default connect(mapState)(Sidebar);