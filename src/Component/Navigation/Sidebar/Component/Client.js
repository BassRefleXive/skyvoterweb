import React from 'react';
import {Menu, Icon} from 'antd';

export default class Client extends React.Component {
    render() {
        return (
            <Menu
                mode="inline"
                style={{height: '100%'}}
            >
                <Menu.Item key="1">
                    <Icon type="pie-chart"/>
                    <span>Client Link 1</span>
                </Menu.Item>
                <Menu.Item key="2">
                    <Icon type="pie-chart"/>
                    <span>Client Link 2</span>
                </Menu.Item>
            </Menu>
        );
    }
}