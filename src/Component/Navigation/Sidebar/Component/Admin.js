import React from 'react';
import { Link } from 'react-router'
import {Menu, Tabs} from 'antd';

import {route} from './../../../../config/constants'

import Client from './Client'

export default class Admin extends React.Component {
    render() {
        return (
            <Tabs defaultActiveKey="admin">
                <Tabs.TabPane tab="Client" key="client">
                    <Client/>
                </Tabs.TabPane>
                <Tabs.TabPane tab="Admin" key="admin">
                    <Menu
                        mode="inline"
                        defaultOpenKeys={['sub1']}
                        style={{height: '100%'}}
                    >
                        <Menu.Item key="top">
                            <span><Link to={`${route.ADMIN.TOP.LIST}`}>Tops</Link></span>
                        </Menu.Item>
                        <Menu.Item key="captcha_resolver">
                            <span><Link to={`${route.ADMIN.CAPTCHA_RESOLVER.LIST}`}>Captcha Resolvers</Link></span>
                        </Menu.Item>
                        <Menu.Item key="user">
                            <span><Link to={`${route.ADMIN.USER.LIST}`}>Users</Link></span>
                        </Menu.Item>
                    </Menu>
                </Tabs.TabPane>
            </Tabs>
        );
    }
}