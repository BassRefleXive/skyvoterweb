import {browserHistory} from 'react-router'
import {Button} from 'antd';

const ButtonLink = ({to, text, type}) => (
    <Button {...Object.assign(
        {},
        type && {type},
        {onClick: () => browserHistory.push(to)}
    )}
    >{text}</Button>
);

export default ButtonLink;