import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {route} from './../../../config/constants'

import {Layout, Menu, Avatar, Card, Button, Popover} from 'antd';
import { Link } from 'react-router'

import {
    logout,
} from "./../../../Flux/User/action/logout";

const mapState = ({
    user: {
        user,
    },
}) => {
    return {
        user,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    logout,
}, dispatch);

class HeaderMenu extends React.Component {
    render() {
        const {
            user,
            logout,
            loading,
        } = this.props;

        const menu = (

            <Card title={user.email} style={{ width: 300 }}>
                <p><Button type="primary" onClick={logout}>Log Out</Button></p>
            </Card>
        );

        return (
            <Layout.Header className="header">
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['1']}
                    style={{lineHeight: '64px'}}
                >
                    {!user.isInitialized() && !loading && (
                        <Menu.Item {...{
                            key: 1,
                        }}>
                            <Link to={`${route.ROUTE_USER_LOGIN}`}>Login</Link>
                        </Menu.Item>
                    )}
                </Menu>
                {user.isInitialized() && !loading && (
                    <div style={style.avatar.container}>
                        <Popover placement="bottomRight" content={menu} trigger="click">
                            <Avatar size="large" icon="user"/>
                        </Popover>
                    </div>
                )}
            </Layout.Header>
        );
    }
}

export default connect(mapState, mapDispatch)(HeaderMenu);

const style = {
    avatar: {
        container: {
            width: 64,
            height: 64,
            float: 'right',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }
    },
};