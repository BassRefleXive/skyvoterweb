import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import 'antd/dist/antd.css';
import HeaderMenu from './../Component/Navigation/Header/HeaderMenu'
import Sidebar from './../Component/Navigation/Sidebar/Sidebar'

import {
    fetchUserData,
    prepareFetchUserDataRequest,
} from "./../Flux/User/action/fetchUserData";

import {RequestStatus} from './../config/constants'

import { Layout } from 'antd';

const mapState = ({
    user: {
        user,
        fetchUserDataRequestStatus,
    },
}) => {
    return {
        user,
        fetchUserDataRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchUserData,
    prepareFetchUserDataRequest,
}, dispatch);

class ApplicationWrapper extends React.Component {
    componentDidMount() {
        const {
            fetchUserData,
            prepareFetchUserDataRequest,
            fetchUserDataRequestStatus,
        } = this.props;

        switch (fetchUserDataRequestStatus) {
            case RequestStatus.VIRGIN:
                fetchUserData();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchUserDataRequest()
        }
    }

    componentWillReceiveProps({
        fetchUserData,
        fetchUserDataRequestStatus,
    }) {
        if (fetchUserDataRequestStatus === RequestStatus.VIRGIN) {
            fetchUserData();
        }
    }

    render() {
        const {
            user,
            fetchUserDataRequestStatus,
        } = this.props;

        return (
            <Layout>
                <HeaderMenu {...{loading: fetchUserDataRequestStatus === RequestStatus.STARTED}} />
                <Layout.Content style={{ padding: '0 50px' }}>
                    <Layout style={{ padding: '24px 24px', background: '#fff' }}>

                        {user.isInitialized() && <Sidebar />}

                        <Layout.Content style={{ padding: '0 24px', minHeight: 280 }}>
                            {this.props.children}
                        </Layout.Content>
                    </Layout>
                </Layout.Content>
            </Layout>
        );
    }
}

export default connect(mapState, mapDispatch)(ApplicationWrapper);