import {stringify} from 'query-string'

export const composeUrl = function (to, pathParams, queryParams) {
    let route = to;
    let hasErrors = false;

    if (pathParams) {
        for (const param in pathParams) {
            if (pathParams[param] === undefined) {
                hasErrors = true
            }

            route = route.replace(`:${param}`, pathParams[param])
        }
    }

    if (hasErrors) {
        console.error(`Composed url from ${to} is ${route}. Some of parameters are missing in`, pathParams)
    }

    if (undefined !== queryParams && Object.keys(queryParams).length) {
        route = `${route}?${stringify(queryParams, {arrayFormat: 'bracket'})}`
    }

    return route
};