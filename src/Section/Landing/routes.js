import {route} from '../../config/constants'
import LandingHandler from './LandingHandler'

export default [
    {
        path: route.ROUTE_LANDING,
        component: LandingHandler,
    },
]