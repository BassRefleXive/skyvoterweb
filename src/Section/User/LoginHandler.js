import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {Card, Row, Col} from 'antd';
import LoginForm from './Component/LoginForm'

import {
    login,
} from "./../../Flux/User/action/login";

import {
    fetchUserData,
} from "./../../Flux/User/action/fetchUserData";

import {RequestStatus} from './../../config/constants'
import {browserHistory} from 'react-router'
import {route} from './../../config/constants'

const mapState = ({
    user: {
        user,
        token,
        loginRequestStatus,
    },
}) => {
    return {
        user,
        token,
        loginRequestStatus
    }
};

const mapDispatch = dispatch => bindActionCreators({
    login,
    fetchUserData,
}, dispatch);

class LoginHandler extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (this.props.loginRequestStatus === RequestStatus.STARTED && nextProps.loginRequestStatus === RequestStatus.FINISHED) {
            this.props.fetchUserData();
        }

        if (nextProps.user.isInitialized()) {
            browserHistory.push(route.ROUTE_LANDING);
        }
    }

    render() {
        const {
            loginRequestStatus,
            login,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={'Login'}>
                        <LoginForm {...{
                            onSubmit: login,
                            loading: loginRequestStatus === RequestStatus.STARTED,
                        }} />
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(LoginHandler);