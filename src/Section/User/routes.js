import {route} from '../../config/constants'
import LoginHandler from './LoginHandler'

export default [
    {
        path: route.ROUTE_USER_LOGIN,
        component: LoginHandler,
    },
]