import Application from '../Application/Application'

import landingRoutes from './Landing/routes'
import userRoutes from './User/routes'

import adminTopRoutes from './Admin/Top/routes'
import adminCaptchaResolverRoutes from './Admin/CaptchaResolver/routes'
import adminUserRoutes from './Admin/User/routes'
import adminServerRoutes from './Admin/Server/routes'
import adminJobRoutes from './Admin/Job/routes'

const routes = [
    {
        path: '',
        component: Application,
        childRoutes: [].concat(
            landingRoutes,
            userRoutes,

            adminTopRoutes,
            adminCaptchaResolverRoutes,
            adminUserRoutes,
            adminServerRoutes,
            adminJobRoutes,
        ),
    },
];

export default routes