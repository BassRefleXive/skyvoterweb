import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Card, Row, Col} from 'antd';

import {RequestStatus} from './../../../config/constants'

import {
    update,
} from "./../../../Flux/Job/action/admin/update";

import {
    fetchOne,
    prepareFetchOneRequest,
} from "./../../../Flux/Job/action/admin/fetchOne";

import {browserHistory} from "react-router";
import {route} from "../../../config/constants";

import Form from './Component/Edit/Form'
import {composeUrl} from "../../../utils/route";

const mapState = ({
                      job: {
                          jobs,
                          changeRequestStatus,
                          fetchRequestStatus,
                      },
                  }, {params: {id}, router: {goBack}}) => {
    return {
        goBack,
        jobId: id,
        job: jobs.get(id),
        changeRequestStatus,
        fetchRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    update,
    fetchOne,
    prepareFetchOneRequest,
}, dispatch);

class EditHandler extends React.Component {
    componentDidMount() {
        const {
            fetchOne,
            prepareFetchOneRequest,
            fetchRequestStatus,
            job,
            jobId,
        } = this.props;

        if (!job) {
            switch (fetchRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchOne(jobId);
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchOneRequest()
            }
        }
    }

    componentWillReceiveProps({
                                  fetchOne,
                                  prepareFetchOneRequest,
                                  fetchRequestStatus,
                                  changeRequestStatus,
                                  job,
                                  jobId,
                              }) {
        if (!job && fetchRequestStatus === RequestStatus.VIRGIN) {
            fetchOne(jobId);
        }

        this.props.changeRequestStatus === RequestStatus.STARTED &&
        changeRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(composeUrl(route.ADMIN.JOB.LIST, {}, {server: job.serverId}));
    }

    render() {
        const {
            job,
            update,
            goBack,
            changeRequestStatus,
            fetchRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Edit Job`}>
                        <Form {...{
                            job,
                            onSubmit: data => update(job.id, data),
                            goBack,
                            loading: changeRequestStatus === RequestStatus.STARTED || fetchRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(EditHandler);