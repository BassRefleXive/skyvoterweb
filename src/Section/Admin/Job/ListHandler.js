import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import JobsTable from './Component/List/JobsTable'
import {Card, Row, Col, Button} from 'antd';
import {browserHistory} from 'react-router'
import {route} from './../../../config/constants'
import {composeUrl} from './../../../utils/route'

import {
    find,
    prepareFindRequest,
} from "./../../../Flux/Job/action/admin/find";

import {
    remove,
} from "./../../../Flux/Job/action/admin/remove";

import {
    update,
} from "./../../../Flux/Job/action/admin/update";

import {fetchOne as fetchServer} from "./../../../Flux/Server/action/admin/fetchOne";

import {RequestStatus} from './../../../config/constants'

const mapState = ({
    server: {
        servers,
        fetchRequestStatus: fetchServerRequestStatus,
    },
    job: {
        jobs,
        fetchRequestStatus: fetchJobsDataRequestStatus,
        changeRequestStatus,
    }
}, {router: {goBack}, location: {query: {server: serverId}}}) => {
    const filteredJobs = undefined === serverId
        ? jobs
        : jobs.filter(job => {
            return job.serverId === serverId;
        });

    return {
        jobs: filteredJobs,
        server: servers.has(serverId) ? servers.get(serverId) : undefined,
        serverId,
        fetchJobsDataRequestStatus,
        fetchServerRequestStatus,
        changeRequestStatus,
        goBack,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    find,
    remove,
    update,
    prepareFindRequest,
    fetchServer,
}, dispatch);

class ListHandler extends React.Component {
    componentDidMount() {
        const {
            serverId,
            jobs,
            find,
            prepareFindRequest,
            fetchJobsDataRequestStatus,
            fetchServerRequestStatus,
            fetchServer,
            server,
        } = this.props;

        switch (fetchJobsDataRequestStatus) {
            case RequestStatus.VIRGIN:
                jobs.isEmpty() && undefined !== serverId && find(serverId);
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFindRequest()
        }

        if (RequestStatus.VIRGIN === fetchServerRequestStatus) {
            undefined === server && undefined !== serverId && fetchServer(serverId);
        }
    }

    componentWillReceiveProps({
        serverId,
        jobs,
        find,
        fetchJobsDataRequestStatus,
        fetchServerRequestStatus,
        fetchServer,
        server,
    }) {
        if (fetchJobsDataRequestStatus === RequestStatus.VIRGIN && jobs.isEmpty()) {
            find(serverId);
        }

        if (RequestStatus.VIRGIN === fetchServerRequestStatus) {
            undefined === server && undefined !== serverId && fetchServer(serverId);
        }
    }

    render() {
        const {
            goBack,
            serverId,
            jobs,
            server,
            fetchJobsDataRequestStatus,
            fetchServerRequestStatus,
            changeRequestStatus,
            remove,
            update,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24}}>
                    <Card title={`List of Jobs`}>
                        <JobsTable {...{
                            jobs,
                            server,
                            remove,
                            update,
                            changeJobRequestStatus:  changeRequestStatus === RequestStatus.STARTED,
                            loading: fetchJobsDataRequestStatus === RequestStatus.STARTED || fetchServerRequestStatus === RequestStatus.STARTED,
                        }} />
                        <Button.Group>
                            <Button type="primary" onClick={ev => browserHistory.push(composeUrl(route.ADMIN.JOB.CREATE, {}, {server: serverId}))}>Create</Button>
                            <Button onClick={goBack}>Back</Button>
                        </Button.Group>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(ListHandler);