import {route} from '../../../config/constants'
import ListHandler from './ListHandler'
import EditHandler from './EditHandler'
import CreateHandler from './CreateHandler'

export default [
    {
        path: route.ADMIN.JOB.LIST,
        component: ListHandler,
    },
    {
        path: route.ADMIN.JOB.EDIT,
        component: EditHandler,
    },
    {
        path: route.ADMIN.JOB.CREATE,
        component: CreateHandler,
    },
]