import React from 'react';
import moment from 'moment';

import {Table, Button, Popconfirm} from 'antd';

import {composeUrl} from './../../../../../utils/route'
import {route} from './../../../../../config/constants'
import ButtonLink from './../../../../../Component/Navigation/ButtonLink'


export default class JobsTable extends React.Component {
    state = {
        changingId: undefined,
    };

    columns() {
        const {
            server,
            changeJobRequestStatus,
        } = this.props;

        return [
            {
                title: 'Date',
                dataIndex: 'date',
                key: 'date',
            },
            {
                title: 'Ordered Votes',
                dataIndex: 'orderedVotes',
                key: 'orderedVotes',
            },
            {
                title: 'Pending Votes',
                dataIndex: 'pendingVotes',
                key: 'pendingVotes',
            },
            {
                title: 'Executed Votes',
                dataIndex: 'executedVotes',
                key: 'executedVotes',
            },
            {
                title: 'Failed Votes',
                dataIndex: 'failedVotes',
                key: 'failedVotes',
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: 'Server',
                dataIndex: 'server',
                key: 'server',
                render: (text, record) => server && server.title || '',
            },
            {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <Button.Group>
                            <ButtonLink {...{
                                type: 'primary',
                                to: composeUrl(route.ADMIN.JOB.EDIT, {id: record.id}),
                                text: 'Edit',
                            }} />
                            <Button {...{
                                loading: changeJobRequestStatus && record.id === this.state.changingId,
                                onClick: () => this.setState({changingId: record.id}) || this.props.update(record.id, {status: record.nextStatus()})
                            }}>Toggle Status</Button>
                            <Popconfirm {...{
                                placement: 'top',
                                title: 'Are you sure?',
                                onConfirm: () => this.props.remove(record.id),
                                yesText: 'Yes',
                                cancelText: 'Cancel',
                            }}>
                                <Button type="danger">Remove</Button>
                            </Popconfirm>
                        </Button.Group>
                    );
                },
            }
        ];
    }

    render() {
        const {
            jobs,
            loading,
        } = this.props;

        return (
            <Table {...{
                columns: this.columns(),
                dataSource: jobs
                    .toArray()
                    .sort((a, b) => moment(a.date).isAfter(moment(b.date)) ? 1 : -1)
                ,
                rowKey: record => record.id,
                loading,
            }} />
        );
    }
}