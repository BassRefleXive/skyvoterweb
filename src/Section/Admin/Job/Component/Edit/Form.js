import React from 'react';
import {Form as AntForm, Input, Button, Select} from 'antd';
import constants from "../../../../../Flux/Job/constants";

class Form extends React.Component {
    componentDidMount() {
        const {
            job,
            form,
        } = this.props;

        if (job) {
            form.setFieldsValue({
                ordered_votes: job.orderedVotes,
                pending_votes: job.pendingVotes,
                executed_votes: job.executedVotes,
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            job,
            form,
        } = nextProps;

        if (job && (!this.props.job || this.props.job.id !== job.id)) {
            form.setFieldsValue({
                ordered_votes: job.orderedVotes,
                pending_votes: job.pendingVotes,
                executed_votes: job.executedVotes,
            });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const {
            goBack,
            loading,
            form: {
                getFieldDecorator,
            }
        } = this.props;

        return (
            <AntForm onSubmit={this.handleSubmit}>

                <AntForm.Item>
                    {getFieldDecorator('ordered_votes', {
                        rules: [{required: true, message: 'Please input ordered votes count!'}],
                    })(
                        <Input {...{
                            placeholder: 'Ordered votes',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('pending_votes', {
                        rules: [{required: true, message: 'Please input pending votes count!'}],
                    })(
                        <Input {...{
                            placeholder: 'Pending votes',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('executed_votes', {
                        rules: [{required: true, message: 'Please input executed votes count!'}],
                    })(
                        <Input {...{
                            placeholder: 'Executed votes',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('status', {
                        initialValue: this.props.job && this.props.job.status,
                        rules: [{required: true, message: 'Please select status!'}],
                    })(
                        <Select>
                            <Select.Option value={constants.JOB_STATUS_ENABLED} key={constants.JOB_STATUS_ENABLED}>Enabled</Select.Option>
                            <Select.Option value={constants.JOB_STATUS_PAUSED} key={constants.JOB_STATUS_PAUSED}>Paused</Select.Option>
                            <Select.Option value={constants.JOB_STATUS_DISABLED} key={constants.JOB_STATUS_DISABLED}>Disabled</Select.Option>
                        </Select>
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    <Button.Group>
                        <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
                        <Button onClick={goBack}>Back</Button>
                    </Button.Group>
                </AntForm.Item>
            </AntForm>
        );
    }
}

export default AntForm.create()(Form);