import React from 'react';
import moment from 'moment';

import {Card, Row, Col, DatePicker} from 'antd';

const Step1 = ({setDates}) => (
    <Row>
        <Col {...{span: 8, offset: 8}}>
            <Card title={`Dates to start end end voting`}>
                <DatePicker.RangePicker {...{
                    ranges: {
                        'This Month': [moment().add('days', 1), moment().endOf('month')],
                        'Next Month': [moment().endOf('month').add('days', 1), moment().add('months', 1).endOf('month')],
                    },
                    disabledDate: current => current && current < moment().endOf('day'),
                    onChange: dates => setDates(dates[0], dates[1]),
                }}/>
            </Card>
        </Col>
    </Row>
);

export default Step1;