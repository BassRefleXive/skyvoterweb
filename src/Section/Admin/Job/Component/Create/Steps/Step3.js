import React from 'react';

import {Icon} from 'antd';

export default class Step3 extends React.Component {
    render() {
        return (
            <div style={styles.icon.container}>
                <Icon {...{
                    type: 'loading',
                    style: styles.icon.icon,
                }} />
                <span style={styles.icon.loading}>Loading...</span>
            </div>
        );
    }
}

const styles = {
    icon: {
        container: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            minHeight: 250,
        },
        icon: {
            fontSize: 54,
            color: '#1890ff',
        },
        loading: {
            fontSize: 12,
            display: 'inline-block',
            marginTop: 10,
            opacity: .75,
        },
    }
};