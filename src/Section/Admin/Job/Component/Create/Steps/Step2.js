import React from 'react';

import {Card, Row, Col, Slider} from 'antd';

import VotesCountSelector from './Step2/VotesCountSelector'

export default class Step2 extends React.Component {
    state = {
        maxDailyVotes: 1000,
        valueToAll: 100,
    };

    onChangeMaxDailyVotes = value => {
        this.setState({
            maxDailyVotes: value,
        });
    };

    changeValueToAll = value => {
        this.props.changeVotesCount(value);

        this.setState({
            valueToAll: value,
        });
    };

    totalVotesCount = () => {
        let count = 0;

        this.props.dates.toArray().map(item => {
            count += item.count;

            return item;
        });

        return count;
    };

    render() {
        const {
            dates,
            changeDateVotesCount,
        } = this.props;

        const {
            maxDailyVotes,
            valueToAll,
        } = this.state;

        const datesObj = dates.toObject();

        return (
            <Row>
                <Col {...{span: 16, offset: 4}}>
                    <Card title={`Configure each day`}>
                        <Row {...{style: styles.row}}>
                            <Col {...{span: 4}}>
                                <span>Max Daily Votes:</span>
                            </Col>
                            <Col {...{span: 20}}>
                                <Slider {...{
                                    min: 10,
                                    max: 10000,
                                    value: maxDailyVotes,
                                    onChange: this.onChangeMaxDailyVotes,
                                }} />
                            </Col>
                        </Row>
                        <Row {...{style: styles.row}}>
                            <Col {...{span: 4}}>
                                <span>Value to all:</span>
                            </Col>
                            <Col {...{span: 20}}>
                                <VotesCountSelector {...{
                                    maxDailyVotes,
                                    onChange: this.changeValueToAll,
                                    value: valueToAll,
                                }} />
                            </Col>
                        </Row>
                        <Row {...{style: styles.row}}>
                            <Col {...{span: 4}}>
                                <span>Total:</span>
                            </Col>
                            <Col {...{span: 20}}>
                                <span>{this.totalVotesCount()}</span>
                            </Col>
                        </Row>

                        <br />
                        <br />

                        {Object.keys(datesObj)
                            .map(idx => {
                            const item = datesObj[idx];

                            return (
                                <Row {...{key: `job-row-${idx}`}}>
                                    <Row {...{style: styles.row}}>
                                        <Col {...{span: 3}}>
                                            <span>{item.date.format('YYYY-MM-DD')}</span>
                                        </Col>
                                        <Col {...{span: 21}}>
                                            <VotesCountSelector {...{
                                                maxDailyVotes,
                                                onChange: count => changeDateVotesCount(item.date, count),
                                                value: item.count,
                                            }} />
                                        </Col>
                                    </Row>
                                </Row>
                            );
                        })}
                    </Card>
                </Col>
            </Row>
        );
    }
}

const styles = {
    row: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
};