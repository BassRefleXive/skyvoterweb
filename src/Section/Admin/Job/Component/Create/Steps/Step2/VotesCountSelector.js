import React from 'react';

import {Slider, InputNumber, Row, Col} from 'antd';

const VotesCountSelector = ({value, maxDailyVotes, onChange}) => (
    <Row>
        <Col span={20}>
            <Slider {...{
                min: 10,
                max: maxDailyVotes,
                value,
                onChange,
            }} />
        </Col>
        <Col span={4}>
            <InputNumber {...{
                min: 10,
                max: maxDailyVotes,
                value,
                style: {
                    marginLeft: 16,
                },
                onChange,
            }} />
        </Col>
    </Row>
);

export default VotesCountSelector;