import React from 'react';
import {Map, Record} from 'immutable';
import moment from 'moment';

import {Button, Steps as AntSteps, message, Icon} from 'antd';

import Step1 from './Steps/Step1'
import Step2 from './Steps/Step2'
import Step3 from './Steps/Step3'

const DEFAULT_DAILY_VOTES = 100;

const JobRecord = Record({
    date: undefined,
    count: DEFAULT_DAILY_VOTES,
});


export default class Steps extends React.Component {
    state = {
        current: 0,
        dates: Map(),
    };

    setDates(start, end) {
        let result = Map();

        for (let m = moment(start); m.isBefore(end) || result.count() === Math.abs(start.diff(end, 'days')); m.add('days', 1)) {
            const date = m.clone();

            result = result.set(date.unix(), new JobRecord({
                date,
            }));
        }

        this.setState({dates: result});
    }

    changeDateVotesCount = (date, count) => {
        const {
            dates,
        } = this.state;

        this.setState({
            dates: dates.set(date.unix(), dates.get(date.unix()).set('count', count)),
        });
    };

    changeVotesCount = count => {
        const {
            dates,
        } = this.state;

        this.setState({
            dates: dates.map(date => {
                return date.set('count', count);
            }),
        });
    };

    next() {
        this.setState({current: ++this.state.current});
    }

    prev() {
        this.setState({current: --this.state.current});
    }

    renderStep() {
        switch (this.state.current) {
            case 0: {
                return <Step1 {...{
                    setDates: (start, end) => this.setDates(start, end),
                }}/>
            }
            case 1: {
                const {
                    dates,
                } = this.state;

                return <Step2 {...{
                    dates,
                    changeDateVotesCount: this.changeDateVotesCount,
                    changeVotesCount: this.changeVotesCount,
                }}/>
            }
            case 2: {
                return <Step3 />
            }
        }
    }

    renderControls() {
        const {
            current,
            dates,
        } = this.state;

        let buttons = [];

        switch (current) {
            case 0: {
                buttons = [
                    {
                        type: !dates.isEmpty() ? 'primary' : 'default',
                        onClick: !dates.isEmpty() ? () => this.next() : () => message.error('Please choose dates first!'),
                        text: 'Next',
                    },
                    {
                        type: 'default',
                        onClick: this.props.goBack,
                        text: 'Back',
                    },
                ];

                break;
            }

            case 1: {
                buttons = [
                    {
                        type: 'primary',
                        onClick: () => this.props.create(this.state.dates.toArray().map(item => {
                            return {
                                date: item.date.format('YYYY-MM-DD'),
                                votes: item.count,
                            };
                        })) || this.next(),
                        text: 'Create',
                    },
                    {
                        type: 'default',
                        onClick: () => this.prev(),
                        text: 'To Dates Select',
                    },
                ];
                break;
            }
        }

        return (
            <Button.Group>
                {buttons.map(({type, onClick, text, loading}, idx) => (
                    <Button {...{
                        key: `control-button-${idx}`,
                        type,
                        onClick
                    }}>{text}</Button>
                ))}
            </Button.Group>
        );
    }

    stepsData() {
        return [
            {
                title: 'Select Dates',
                description: 'Select dates to create jobs to',
            }, {
                title: 'Configure',
                description: 'Choose votes count for each date',
            }, Object.assign(
                {
                    title: 'Creating...',
                    description: '',
                },
                this.props.loading && {icon: 'loading'}
            )
        ];
    }

    render() {

        return (
            <div>
                <AntSteps {...{
                    current: this.state.current,
                    style: {
                        marginBottom: 50,
                    }
                }}>
                    {this.stepsData().map(item => <AntSteps.Step {...Object.assign(
                        {
                            key: item.title,
                            title: item.title,
                            description: item.description,
                        },
                        undefined !== item.icon && {
                            icon: <Icon {...{type: item.icon}} />
                        }
                    )} />)}
                </AntSteps>
                <div className="steps-content">{this.renderStep()}</div>
                {this.renderControls()}
            </div>
        );
    }
}