import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Card, Row, Col} from 'antd';
import {browserHistory} from "react-router";

import {RequestStatus} from './../../../config/constants'

import {route} from "../../../config/constants";
import {composeUrl} from './../../../utils/route'

import {create} from "./../../../Flux/Job/action/admin/create";

import {fetchOne as fetchServer} from "./../../../Flux/Server/action/admin/fetchOne";

import Steps from './Component/Create/Steps'

const mapState = ({
    server: {
        servers,
        fetchRequestStatus: fetchServerRequestStatus,
    },
    job: {
        createRequestStatus,
    },
}, {router: {goBack}, location: {query: {server: serverId}}}) => {
    return {
        serverId,
        server: servers.has(serverId) ? servers.get(serverId) : undefined,
        goBack,
        fetchServerRequestStatus,
        createRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    create,
    fetchServer,
}, dispatch);

class CreateHandler extends React.Component {
    componentDidMount() {
        const {
            serverId,
            fetchServerRequestStatus,
            fetchServer,
            server,
        } = this.props;

        if (RequestStatus.VIRGIN === fetchServerRequestStatus) {
            undefined === server && undefined !== serverId && fetchServer(serverId);
        }
    }

    componentWillReceiveProps({
        serverId,
        fetchServerRequestStatus,
        createRequestStatus,
        fetchServer,
        server,
    }) {
        this.props.createRequestStatus === RequestStatus.STARTED &&
        createRequestStatus !== RequestStatus.STARTED &&
        setTimeout(() => browserHistory.push(composeUrl(route.ADMIN.JOB.LIST, {}, {server: serverId})), 1000);

        if (RequestStatus.VIRGIN === fetchServerRequestStatus) {
            undefined === server && undefined !== serverId && fetchServer(serverId);
        }
    }

    createJobs = jobsData => {
        const {
            create,
            serverId,
        } = this.props;

        create(serverId, jobsData);
    };

    render() {
        const {
            goBack,
            createRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24, offset: 0}}>
                    <Card title={`Add New Jobs`}>
                        <Steps {...{
                            goBack,
                            create: this.createJobs,
                            loading: createRequestStatus === RequestStatus.STARTED,
                        }} />
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(CreateHandler);