import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {RequestStatus} from './../../../config/constants'

import {
    fetchAll as fetchCaptchaResolversData,
    prepareFetchAllRequest as prepareFetchCaptchaResolversDataRequest,
} from "./../../../Flux/CaptchaResolver/action/admin/fetchAll";

import CreateTop from './Component/Create/CreateTop'

const mapState = ({
    captchaResolver: {
        resolvers,
        fetchRequestStatus: fetchCaptchaResolversDataRequestStatus,
    },
}, {router: {goBack}}) => {
    return {
        goBack,
        resolvers,
        fetchCaptchaResolversDataRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchCaptchaResolversData,
    prepareFetchCaptchaResolversDataRequest,
}, dispatch);

class CreateTopHandler extends React.Component {
    componentDidMount() {
        const {
            resolvers,
            fetchCaptchaResolversData,
            prepareFetchCaptchaResolversDataRequest,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        if (resolvers.isEmpty()) {
            switch (fetchCaptchaResolversDataRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchCaptchaResolversData();
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchCaptchaResolversDataRequest()
            }
        }
    }

    componentWillReceiveProps({
        resolvers,
        fetchCaptchaResolversData,
        fetchCaptchaResolversDataRequestStatus,
    }) {
        if (resolvers.isEmpty() && fetchCaptchaResolversDataRequestStatus === RequestStatus.VIRGIN) {
            fetchCaptchaResolversData();
        }
    }

    render() {
        const {
            goBack,
            resolvers,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        console.log(top);

        return (
            <CreateTop {...{
                resolvers,
                loading: fetchCaptchaResolversDataRequestStatus === RequestStatus.STARTED,
                goBack,
            }} />
        );
    }
}

export default connect(mapState, mapDispatch)(CreateTopHandler);