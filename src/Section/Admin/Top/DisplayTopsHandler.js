import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import TopsTable from './Component/List/TopsTable'
import {Card, Row, Col, Button} from 'antd';
import {browserHistory} from 'react-router'
import {route} from './../../../config/constants'

import {
    fetchTopsData,
    prepareFetchTopsDataRequest,
} from "./../../../Flux/Top/action/admin/fetchTopsData";

import {
    fetchAll as fetchCaptchaResolversData,
    prepareFetchAllRequest as prepareFetchCaptchaResolversDataRequest,
} from "./../../../Flux/CaptchaResolver/action/admin/fetchAll";

import {
    removeTop,
} from "./../../../Flux/Top/action/admin/removeTop";

import {RequestStatus} from './../../../config/constants'

const mapState = ({
                      tops: {
                          tops,
                          fetchRequestStatus: fetchTopsDataRequestStatus,
                      },
                      captchaResolver: {
                          resolvers,
                          fetchRequestStatus: fetchCaptchaResolversDataRequestStatus,
                      }
                  }) => {
    return {
        tops,
        fetchTopsDataRequestStatus,
        resolvers,
        fetchCaptchaResolversDataRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    removeTop,
    fetchTopsData,
    prepareFetchTopsDataRequest,
    fetchCaptchaResolversData,
    prepareFetchCaptchaResolversDataRequest,
}, dispatch);

class DisplayTopsHandler extends React.Component {
    componentDidMount() {
        const {
            tops,
            resolvers,
            fetchTopsData,
            prepareFetchTopsDataRequest,
            fetchTopsDataRequestStatus,
            fetchCaptchaResolversData,
            prepareFetchCaptchaResolversDataRequest,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        switch (fetchTopsDataRequestStatus) {
            case RequestStatus.VIRGIN:
                tops.isEmpty() && fetchTopsData();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchTopsDataRequest()
        }

        switch (fetchCaptchaResolversDataRequestStatus) {
            case RequestStatus.VIRGIN:
                resolvers.isEmpty() && fetchCaptchaResolversData();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchCaptchaResolversDataRequest()
        }
    }

    componentWillReceiveProps({
                                  tops,
                                  resolvers,
                                  fetchTopsData,
                                  fetchTopsDataRequestStatus,
                                  fetchCaptchaResolversData,
                                  fetchCaptchaResolversDataRequestStatus,
                              }) {
        if (fetchTopsDataRequestStatus === RequestStatus.VIRGIN && tops.isEmpty()) {
            fetchTopsData();
        }

        if (fetchCaptchaResolversDataRequestStatus === RequestStatus.VIRGIN && resolvers.isEmpty()) {
            fetchCaptchaResolversData();
        }
    }

    render() {
        const {
            tops,
            fetchTopsDataRequestStatus,
            resolvers,
            fetchCaptchaResolversDataRequestStatus,
            removeTop,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24}}>
                    <Card title={`List of Tops`}>
                        <TopsTable {...{
                            tops,
                            resolvers,
                            loading: fetchTopsDataRequestStatus === RequestStatus.STARTED || fetchCaptchaResolversDataRequestStatus === RequestStatus.STARTED,
                            removeTop,
                        }} />
                        <Button type="primary"
                                onClick={ev => browserHistory.push(route.ADMIN.TOP.CREATE)}>Create</Button>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(DisplayTopsHandler);

const css = {
    component: {
        container: {
            width: '100%',
            textAlign: 'center',
        },
    },
};