import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {RequestStatus} from './../../../config/constants'

import {
    fetchTopData,
    prepareFetchTopDataRequest,
} from './../../../Flux/Top/action/admin/fetchTopData'

import {
    fetchAll as fetchCaptchaResolversData,
    prepareFetchAllRequest as prepareFetchCaptchaResolversDataRequest,
} from "./../../../Flux/CaptchaResolver/action/admin/fetchAll";

import EditTop from './Component/Edit/EditTop'

const mapState = ({
    tops: {
        tops,
        fetchRequestStatus: fetchTopDataRequestStatus,
    },
    captchaResolver: {
        resolvers,
        fetchRequestStatus: fetchCaptchaResolversDataRequestStatus,
    },
}, {params: {id}, router: {goBack}}) => {
    return {
        goBack,
        topId: id,
        top: tops.get(id),
        fetchTopDataRequestStatus,
        resolvers,
        fetchCaptchaResolversDataRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchTopData,
    prepareFetchTopDataRequest,
    fetchCaptchaResolversData,
    prepareFetchCaptchaResolversDataRequest,
}, dispatch);

class EditTopHandler extends React.Component {
    componentDidMount() {
        const {
            fetchTopData,
            fetchTopDataRequestStatus,
            top,
            topId,
            resolvers,
            fetchCaptchaResolversData,
            prepareFetchCaptchaResolversDataRequest,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        if (!top) {
            switch (fetchTopDataRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchTopData(topId);
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchTopDataRequest()
            }
        }

        if (resolvers.isEmpty()) {
            switch (fetchCaptchaResolversDataRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchCaptchaResolversData();
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchCaptchaResolversDataRequest()
            }
        }
    }

    componentWillReceiveProps({
        fetchTopData,
        fetchTopDataRequestStatus,
        top,
        topId,
        resolvers,
        fetchCaptchaResolversData,
        fetchCaptchaResolversDataRequestStatus,
    }) {
        if (!top && fetchTopDataRequestStatus === RequestStatus.VIRGIN) {
            fetchTopData(topId);
        }

        if (resolvers.isEmpty() && fetchCaptchaResolversDataRequestStatus === RequestStatus.VIRGIN) {
            fetchCaptchaResolversData();
        }
    }

    render() {
        const {
            goBack,
            top,
            resolvers,
            fetchTopDataRequestStatus,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        console.log(top);

        return (
            <EditTop {...{
                goBack,
                top,
                resolvers,
                loading: fetchTopDataRequestStatus === RequestStatus.STARTED || fetchCaptchaResolversDataRequestStatus === RequestStatus.STARTED,
            }} />
        );
    }
}

export default connect(mapState, mapDispatch)(EditTopHandler);