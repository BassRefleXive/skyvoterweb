import {route} from '../../../config/constants'
import DisplayTopsHandler from './DisplayTopsHandler'
import EditTopHandler from './EditTopHandler'
import CreateTopHandler from './CreateTopHandler'

export default [
    {
        path: route.ADMIN.TOP.LIST,
        component: DisplayTopsHandler,
    },
    {
        path: route.ADMIN.TOP.EDIT,
        component: EditTopHandler,
    },
    {
        path: route.ADMIN.TOP.CREATE,
        component: CreateTopHandler,
    },
]