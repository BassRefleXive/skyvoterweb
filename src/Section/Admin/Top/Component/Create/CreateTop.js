import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router'
import {Card, Row, Col} from 'antd';

import TopForm from './../TopForm'

import {
    createTop,
} from "./../../../../../Flux/Top/action/admin/createTop";


import {RequestStatus} from './../../../../../config/constants'
import {route} from './../../../../../config/constants'

const mapState = ({
    tops: {
        createRequestStatus,
    },
}) => {
    return {
        createRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    createTop,
}, dispatch);

class CreateTop extends React.Component {
    handleSubmit = (values) => {
        this.props.createTop(values);
    };

    componentWillReceiveProps(nextProps) {
        this.props.createRequestStatus === RequestStatus.STARTED &&
        nextProps.createRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(route.ADMIN.TOP.LIST);
    }

    render() {
        const {
            goBack,
            resolvers,
            loading,
            createRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Add New Top`}>
                        <TopForm {...{
                            onSubmit: this.handleSubmit,
                            goBack,
                            resolvers,
                            loading: loading || createRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(CreateTop);