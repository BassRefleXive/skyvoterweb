import React from 'react';
import {Table, Button, Popconfirm} from 'antd';
import {composeUrl} from './../../../../../utils/route'
import {route} from './../../../../../config/constants'
import ButtonLink from './../../../../../Component/Navigation/ButtonLink'

export default class TopsTable extends React.Component {

    columns() {
        const {
            resolvers,
        } = this.props;

        return [
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'title',
            },
            {
                title: 'Code',
                dataIndex: 'code',
                key: 'code',
            },
            {
                title: 'Multiplier',
                dataIndex: 'multiplier',
                key: 'multiplier',
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: 'Captcha Resolver',
                dataIndex: 'captchaResolverId',
                key: 'captchaResolverId',
                render: (text, record) => resolvers.has(record.captchaResolverId) ? resolvers.get(record.captchaResolverId).code : ''
            }, {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    const {editable} = record;

                    return (
                        <Button.Group>
                            <ButtonLink {...{
                                to: composeUrl(route.ADMIN.TOP.EDIT, {id: record.id}),
                                type: 'primary',
                                text: 'Edit',
                            }} />
                            <Popconfirm {...{
                                placement: 'top',
                                title: 'Are you sure?',
                                onConfirm: () => this.props.removeTop(record.id),
                                yesText: 'Yes',
                                cancelText: 'Cancel',
                            }}>
                                <Button type="danger">Remove</Button>
                            </Popconfirm>
                        </Button.Group>
                    );
                },
            }
        ];
    }

    render() {
        const {
            tops,
            loading,
        } = this.props;

        return (
            <Table {...{
                columns: this.columns(),
                dataSource: tops.toArray(),
                rowKey: record => record.id,
                loading,
            }} />
        );
    }
}