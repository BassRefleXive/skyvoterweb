import React from 'react';
import {Form, Input, Button, Select, Row, Col} from 'antd';

import constants from './../../../../Flux/Top/constants'
import {RequestStatus, route} from "../../../../config/constants";
import {browserHistory} from "react-router";

class TopForm extends React.Component {
    componentDidMount() {
        const {
            top,
            form,
        } = this.props;

        if (top) {
            form.setFieldsValue({
                title: top.title,
                code: top.code,
                multiplier: top.multiplier,
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            top,
            form,
        } = nextProps;

        if (top && (!this.props.top || this.props.top.id !== top.id)) {
            form.setFieldsValue({
                title: top.title,
                code: top.code,
                multiplier: top.multiplier,
            });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const {
            goBack,
            top,
            resolvers,
            loading,
            form: {
                getFieldDecorator,
            }
        } = this.props;

        return (
            <Form onSubmit={this.handleSubmit}>

                <Form.Item>
                    {getFieldDecorator('title', {
                        rules: [{required: true, message: 'Please input top title!'}],
                    })(
                        <Input {...{
                            placeholder: 'Title',
                        }} />
                    )}
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator('code', {
                        rules: [{required: true, message: 'Please input top code!'}],
                    })(
                        <Input {...{
                            placeholder: 'Code',
                        }} />
                    )}
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator('multiplier', {
                        rules: [{required: true, message: 'Please input top votes multiplier!'}],
                    })(
                        <Input {...{
                            placeholder: 'Multiplier',
                            type: 'number',
                        }} />
                    )}
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator('status', {
                        initialValue: top && top.status,
                        rules: [{required: true, message: 'Please select status!'}],
                    })(
                        <Select>
                            <Select.Option value={constants.TOP_STATUS_ENABLED} key={constants.TOP_STATUS_ENABLED}>Enabled</Select.Option>
                            <Select.Option value={constants.TOP_STATUS_PAUSED}
                                           key={constants.TOP_STATUS_PAUSED}>Paused</Select.Option>
                            <Select.Option value={constants.TOP_STATUS_DISABLED} key={constants.TOP_STATUS_DISABLED}>Disabled</Select.Option>
                        </Select>
                    )}
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator('captcha_resolver', {
                        initialValue: top && top.captchaResolverId,
                        rules: [{required: true, message: 'Please select Captcha Resolver!'}],
                    })(
                        <Select>
                            {resolvers.toArray().map((resolver) => (
                                    <Select.Option value={resolver.id} key={resolver.id}>{resolver.code}</Select.Option>
                                )
                            )}
                        </Select>
                    )}
                </Form.Item>

                <Form.Item>
                    <Button.Group>
                        <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
                        <Button onClick={goBack}>Back</Button>
                    </Button.Group>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create()(TopForm);