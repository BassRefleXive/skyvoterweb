import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router'
import {Card, Row, Col} from 'antd';

import TopForm from './../TopForm'

import {
    updateTop,
} from "./../../../../../Flux/Top/action/admin/updateTop";


import {RequestStatus} from './../../../../../config/constants'
import {route} from './../../../../../config/constants'

const mapState = ({
    tops: {
        changeRequestStatus,
    },
}) => {
    return {
        changeRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    updateTop,
}, dispatch);

class EditTopForm extends React.Component {
    handleSubmit = (values) => {
        this.props.updateTop(this.props.top.id, values);
    };

    componentWillReceiveProps(nextProps) {
        this.props.changeRequestStatus === RequestStatus.STARTED &&
        nextProps.changeRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(route.ADMIN.TOP.LIST);
    }

    render() {
        const {
            goBack,
            top,
            resolvers,
            loading,
            changeRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Edit Top ${top && top.title || ''}`}>
                        <TopForm {...{
                            onSubmit: this.handleSubmit,
                            goBack,
                            top,
                            resolvers,
                            loading: loading || changeRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(EditTopForm);