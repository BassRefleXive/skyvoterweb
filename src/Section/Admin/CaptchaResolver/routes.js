import {route} from '../../../config/constants'
import ListHandler from './ListHandler'
import EditHandler from './EditHandler'
import CreateHandler from './CreateHandler'

export default [
    {
        path: route.ADMIN.CAPTCHA_RESOLVER.LIST,
        component: ListHandler,
    },
    {
        path: route.ADMIN.CAPTCHA_RESOLVER.EDIT,
        component: EditHandler,
    },
    {
        path: route.ADMIN.CAPTCHA_RESOLVER.CREATE,
        component: CreateHandler,
    },
]