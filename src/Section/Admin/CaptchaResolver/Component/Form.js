import React from 'react';
import {Form as AntForm, Input, Button} from 'antd';

class Form extends React.Component {
    componentDidMount() {
        const {
            resolver,
            form,
        } = this.props;

        if (resolver) {
            form.setFieldsValue({
                code: resolver.code,
                key: resolver.key,
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            resolver,
            form,
        } = nextProps;

        if (resolver && (!this.props.resolver || this.props.resolver.id !== resolver.id)) {
            form.setFieldsValue({
                code: resolver.code,
                key: resolver.key,
            });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const {
            goBack,
            loading,
            form: {
                getFieldDecorator,
            }
        } = this.props;

        return (
            <AntForm onSubmit={this.handleSubmit}>

                <AntForm.Item>
                    {getFieldDecorator('code', {
                        rules: [{required: true, message: 'Please input captcha resolver code!'}],
                    })(
                        <Input {...{
                            placeholder: 'Code',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('key', {
                        rules: [{required: true, message: 'Please input captcha resolver key!'}],
                    })(
                        <Input {...{
                            placeholder: 'Key',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    <Button.Group>
                        <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
                        <Button onClick={goBack}>Back</Button>
                    </Button.Group>
                </AntForm.Item>
            </AntForm>
        );
    }
}

export default AntForm.create()(Form);