import React from 'react';
import {Table, Button, Popconfirm} from 'antd';
import {composeUrl} from './../../../../../utils/route'
import {route} from './../../../../../config/constants'
import ButtonLink from './../../../../../Component/Navigation/ButtonLink'

export default class CaptchaResolversTable extends React.Component {

    columns() {
        return [
            {
                title: 'Code',
                dataIndex: 'code',
                key: 'code',
            },
            {
                title: 'Key',
                dataIndex: 'key',
                key: 'key',
            }, {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <Button.Group>
                            <ButtonLink {...{
                                to: composeUrl(route.ADMIN.CAPTCHA_RESOLVER.EDIT, {id: record.id}),
                                type: 'primary',
                                text: 'Edit',
                            }} />
                            <Popconfirm {...{
                                placement: 'top',
                                title: 'Are you sure?',
                                onConfirm: () => this.props.remove(record.id),
                                yesText: 'Yes',
                                cancelText: 'Cancel',
                            }}>
                                <Button type="danger">Remove</Button>
                            </Popconfirm>
                        </Button.Group>
                    );
                },
            }
        ];
    }

    render() {
        const {
            resolvers,
            loading,
        } = this.props;

        return (
            <Table {...{
                columns: this.columns(),
                dataSource: resolvers.toArray(),
                rowKey: record => record.id,
                loading,
            }} />
        );
    }
}