import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import CaptchaResolversTable from './Component/List/CaptchaResolversTable'
import {Card, Row, Col, Button} from 'antd';
import {browserHistory} from 'react-router'
import {route} from './../../../config/constants'

import {
    fetchAll,
    prepareFetchAllRequest,
} from "./../../../Flux/CaptchaResolver/action/admin/fetchAll";

import {
    remove,
} from "./../../../Flux/CaptchaResolver/action/admin/remove";

import {RequestStatus} from './../../../config/constants'

const mapState = ({
                      captchaResolver: {
                          resolvers,
                          fetchRequestStatus: fetchCaptchaResolversDataRequestStatus,
                      }
                  }) => {
    return {
        resolvers,
        fetchCaptchaResolversDataRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchAll,
    prepareFetchAllRequest,
    remove,
}, dispatch);

class ListHandler extends React.Component {
    componentDidMount() {
        const {
            resolvers,
            fetchAll,
            prepareFetchAllRequest,
            fetchCaptchaResolversDataRequestStatus,
        } = this.props;

        switch (fetchCaptchaResolversDataRequestStatus) {
            case RequestStatus.VIRGIN:
                resolvers.isEmpty() && fetchAll();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchAllRequest()
        }
    }

    componentWillReceiveProps({
                                  resolvers,
                                  fetchAll,
                                  prepareFetchAllRequest,
                                  fetchCaptchaResolversDataRequestStatus,
                              }) {
        if (fetchCaptchaResolversDataRequestStatus === RequestStatus.VIRGIN && resolvers.isEmpty()) {
            fetchAll();
        }
    }

    render() {
        const {
            resolvers,
            fetchCaptchaResolversDataRequestStatus,
            remove,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24}}>
                    <Card title={`List of Captcha Resolvers`}>
                        <CaptchaResolversTable {...{
                            resolvers,
                            loading: fetchCaptchaResolversDataRequestStatus === RequestStatus.STARTED,
                            remove,
                        }} />
                        <Button type="primary" onClick={ev => browserHistory.push(route.ADMIN.CAPTCHA_RESOLVER.CREATE)}>Create</Button>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(ListHandler);