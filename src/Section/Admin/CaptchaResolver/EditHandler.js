import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Card, Row, Col} from 'antd';

import {RequestStatus} from './../../../config/constants'

import {
    update,
} from "./../../../Flux/CaptchaResolver/action/admin/update";

import {
    fetchOne,
    prepareFetchOneRequest,
} from "./../../../Flux/CaptchaResolver/action/admin/fetchOne";

import {browserHistory} from "react-router";
import {route} from "../../../config/constants";

import Form from './Component/Form'

const mapState = ({
                      captchaResolver: {
                          resolvers,
                          changeRequestStatus,
                          fetchRequestStatus,
                      },
                  }, {params: {id}, router: {goBack}}) => {
    return {
        goBack,
        resolverId: id,
        resolver: resolvers.get(id),
        changeRequestStatus,
        fetchRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    update,
    fetchOne,
    prepareFetchOneRequest,
}, dispatch);

class EditHandler extends React.Component {
    componentDidMount() {
        const {
            fetchOne,
            prepareFetchOneRequest,
            fetchRequestStatus,
            resolver,
            resolverId,
        } = this.props;

        if (!resolver) {
            switch (fetchRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchOne(resolverId);
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchOneRequest()
            }
        }
    }

    componentWillReceiveProps({
                                  fetchOne,
                                  prepareFetchOneRequest,
                                  fetchRequestStatus,
                                  changeRequestStatus,
                                  resolver,
                                  resolverId,
                              }) {
        if (!resolver && fetchRequestStatus === RequestStatus.VIRGIN) {
            fetchOne(resolverId);
        }

        this.props.changeRequestStatus === RequestStatus.STARTED &&
        changeRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(route.ADMIN.CAPTCHA_RESOLVER.LIST);
    }

    render() {
        const {
            resolver,
            update,
            goBack,
            changeRequestStatus,
            fetchRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Edit Captcha Resolver ${resolver && resolver.code || ''}`}>
                        <Form {...{
                            resolver,
                            onSubmit: data => update(resolver.id, data),
                            goBack,
                            loading: changeRequestStatus === RequestStatus.STARTED || fetchRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(EditHandler);