import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Card, Row, Col} from 'antd';

import {RequestStatus} from './../../../config/constants'
import {composeUrl} from './../../../utils/route'

import {
    create,
} from "./../../../Flux/Server/action/admin/create";

import {
    fetchTopsData,
    prepareFetchTopsDataRequest,
} from "./../../../Flux/Top/action/admin/fetchTopsData";

import {browserHistory} from "react-router";
import {route} from "../../../config/constants";

import Form from './Component/Form'

const mapState = ({
    tops: {
        tops,
        fetchRequestStatus: fetchTopsDataRequestStatus,
    },
    server: {
        createRequestStatus,
    },
}, {router: {goBack}, location: {query: {owner: ownerId}}}) => {
    return {
        ownerId,
        goBack,
        tops,
        fetchTopsDataRequestStatus,
        createRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    create,
    fetchTopsData,
    prepareFetchTopsDataRequest,
}, dispatch);

class CreateHandler extends React.Component {
    componentDidMount() {
        const {
            tops,
            fetchTopsDataRequestStatus,
            fetchTopsData,
            prepareFetchTopsDataRequest,
        } = this.props;

        if (tops.isEmpty()) {
            switch (fetchTopsDataRequestStatus) {
                case RequestStatus.VIRGIN:
                    fetchTopsData();
                    break;
                case RequestStatus.STARTED:
                    break;
                default:
                    prepareFetchTopsDataRequest()
            }
        }
    }

    componentWillReceiveProps({
        ownerId,
        tops,
        fetchTopsDataRequestStatus,
        createRequestStatus,
    }) {
        this.props.createRequestStatus === RequestStatus.STARTED &&
        createRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(composeUrl(route.ADMIN.SERVER.LIST, {}, {owner: ownerId}));

        if (tops.isEmpty() && fetchTopsDataRequestStatus === RequestStatus.VIRGIN) {
            fetchTopsData();
        }
    }

    render() {
        const {
            ownerId,
            tops,
            create,
            goBack,
            createRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Add New Server`}>
                        <Form {...{
                            tops,
                            onSubmit: data => create(Object.assign({}, data, {owner_id: ownerId})),
                            goBack,
                            loading: createRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(CreateHandler);