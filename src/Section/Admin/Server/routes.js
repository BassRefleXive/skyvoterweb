import {route} from '../../../config/constants'
import ListHandler from './ListHandler'
import CreateHandler from './CreateHandler'

export default [
    {
        path: route.ADMIN.SERVER.LIST,
        component: ListHandler,
    },
    {
        path: route.ADMIN.SERVER.CREATE,
        component: CreateHandler,
    },
]