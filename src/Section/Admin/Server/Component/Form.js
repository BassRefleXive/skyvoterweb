import React from 'react';
import {Form as AntForm, Input, Button, Row, Col, Select} from 'antd';

class Form extends React.Component {
    componentDidMount() {
        const {
            server,
            form,
        } = this.props;

        server && form.setFieldsValue({
            title: server.title,
            provider_id: server.providerId,
        });
    }

    componentWillReceiveProps(nextProps) {
        const {
            server,
            form,
        } = nextProps;

        if (server && (!this.props.server || this.props.server.id !== server.id)) {
            form.setFieldsValue({
                title: server.title,
                provider_id: server.providerId,
            });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const {
            tops,
            server,
            goBack,
            loading,
            form: {
                getFieldDecorator,
            }
        } = this.props;

        return (
            <AntForm onSubmit={this.handleSubmit}>

                <AntForm.Item>
                    {getFieldDecorator('title', {
                        rules: [{required: true, message: 'Please input title!'}],
                    })(
                        <Input {...{
                            placeholder: 'title',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('provider_id', {
                        rules: [{required: true, message: 'Please input provider ID!'}],
                    })(
                        <Input {...{
                            placeholder: 'provider_id',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('top_id', {
                        initialValue: server && server.topId,
                        rules: [{required: true, message: 'Please select Top!'}],
                    })(
                        <Select>
                            {tops.toArray().map(top => (
                                    <Select.Option value={top.id} key={top.id}>{top.title}</Select.Option>
                                )
                            )}
                        </Select>
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    <Button.Group>
                        <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
                        <Button onClick={goBack}>Back</Button>
                    </Button.Group>
                </AntForm.Item>
            </AntForm>
        );
    }
}

export default AntForm.create()(Form);