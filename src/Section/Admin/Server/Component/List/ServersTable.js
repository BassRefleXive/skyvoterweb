import React from 'react';
import {Table, Button, Popconfirm} from 'antd';
import {route} from './../../../../../config/constants'
import {composeUrl} from './../../../../../utils/route'
import {browserHistory} from 'react-router'

export default class ServersTable extends React.Component {
    state = {
        changingId: undefined,
    };

    columns() {
        const {
            tops,
            changeServerRequestStatus,
        } = this.props;

        return [
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'title',
            },
            {
                title: 'Provider ID',
                dataIndex: 'providerId',
                key: 'providerId',
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: 'Top',
                dataIndex: 'top',
                key: 'top',
                render: (text, record) => tops.has(record.topId) ? tops.get(record.topId).title : ''
            }, {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <Button.Group>
                            <Button {...{
                                loading: changeServerRequestStatus && record.id === this.state.changingId,
                                type: 'primary',
                                onClick: () => this.setState({changingId: record.id}) || this.props.update(record.id, {status: record.nextStatus()})
                            }}>Toggle Status</Button>
                            <Button {...{
                                onClick: ev => browserHistory.push(composeUrl(route.ADMIN.JOB.LIST, {}, {server: record.id}))
                            }}>Jobs</Button>
                            <Popconfirm {...{
                                placement: 'top',
                                title: 'Are you sure?',
                                onConfirm: () => this.props.remove(record.id),
                                yesText: 'Yes',
                                cancelText: 'Cancel',
                            }}>
                                <Button type="danger">Remove</Button>
                            </Popconfirm>
                        </Button.Group>
                    );
                },
            }
        ];
    }

    render() {
        const {
            servers,
            loading,
        } = this.props;

        return (
            <Table {...{
                columns: this.columns(),
                dataSource: servers.toArray(),
                rowKey: record => record.id,
                loading,
            }} />
        );
    }
}