import React from 'react';
import {Map} from "immutable"
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ServersTable from './Component/List/ServersTable'
import {Card, Row, Col, Button} from 'antd';
import {browserHistory} from 'react-router'
import {route} from './../../../config/constants'
import {composeUrl} from './../../../utils/route'

import {
    find,
    prepareFindRequest,
} from "./../../../Flux/Server/action/admin/find";

import {
    remove,
} from "./../../../Flux/Server/action/admin/remove";

import {
    update,
} from "./../../../Flux/Server/action/admin/update";

import {
    fetchTopsData,
    prepareFetchTopsDataRequest,
} from "./../../../Flux/Top/action/admin/fetchTopsData";

import {RequestStatus} from './../../../config/constants'

const mapState = ({
    tops: {
        tops,
        fetchRequestStatus: fetchTopsDataRequestStatus,
    },
    server: {
        servers,
        fetchRequestStatus: fetchServersDataRequestStatus,
        changeRequestStatus: changeServerRequestStatus,
    }
}, {router: {goBack}, location: {query: {owner: ownerId}}}) => {
    const filteredServers = undefined === ownerId
        ? servers
        : servers.filter(server => {
            return server.ownerId === ownerId;
        });

    return {
        goBack,
        tops,
        ownerId,
        servers: filteredServers,
        fetchServersDataRequestStatus,
        fetchTopsDataRequestStatus,
        changeServerRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    find,
    prepareFindRequest,
    update,
    remove,
    fetchTopsData,
    prepareFetchTopsDataRequest,
}, dispatch);

class ListHandler extends React.Component {
    componentDidMount() {
        const {
            ownerId,
            servers,
            find,
            prepareFindRequest,
            fetchServersDataRequestStatus,
            fetchTopsData,
            prepareFetchTopsDataRequest,
            fetchTopsDataRequestStatus,
            tops,
        } = this.props;

        switch (fetchServersDataRequestStatus) {
            case RequestStatus.VIRGIN:
                servers.isEmpty() && find([], ownerId);
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFindRequest()
        }

        switch (fetchTopsDataRequestStatus) {
            case RequestStatus.VIRGIN:
                tops.isEmpty() && fetchTopsData();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchTopsDataRequest()
        }
    }

    componentWillReceiveProps({
        ownerId,
        servers,
        find,
        fetchServersDataRequestStatus,
        fetchTopsData,
        fetchTopsDataRequestStatus,
        tops,
    }) {
        if (fetchServersDataRequestStatus === RequestStatus.VIRGIN && servers.isEmpty()) {
            find([], ownerId);
        }

        if (fetchTopsDataRequestStatus === RequestStatus.VIRGIN && tops.isEmpty()) {
            fetchTopsData();
        }
    }

    render() {
        const {
            goBack,
            ownerId,
            servers,
            fetchServersDataRequestStatus,
            remove,
            update,
            tops,
            fetchTopsDataRequestStatus,
            changeServerRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24}}>
                    <Card title={`List of Servers`}>
                        <ServersTable {...{
                            tops,
                            servers,
                            loading: fetchServersDataRequestStatus === RequestStatus.STARTED || fetchTopsDataRequestStatus === RequestStatus.STARTED,
                            remove,
                            update,
                            changeServerRequestStatus: changeServerRequestStatus === RequestStatus.STARTED,
                        }} />
                        <Button.Group>
                            <Button type="primary" onClick={ev => browserHistory.push(composeUrl(route.ADMIN.SERVER.CREATE, {}, {owner: ownerId}))}>Create</Button>
                            <Button onClick={goBack}>Back</Button>
                        </Button.Group>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(ListHandler);