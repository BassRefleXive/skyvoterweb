import {route} from '../../../config/constants'
import ListHandler from './ListHandler'
// import EditHandler from './EditHandler'
import CreateHandler from './CreateHandler'

export default [
    {
        path: route.ADMIN.USER.LIST,
        component: ListHandler,
    },
    {
        path: route.ADMIN.USER.CREATE,
        component: CreateHandler,
    },
]