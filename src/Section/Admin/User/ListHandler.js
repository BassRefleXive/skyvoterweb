import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import UsersTable from './Component/List/UsersTable'
import {Card, Row, Col, Button} from 'antd';
import {browserHistory} from 'react-router'
import {route} from './../../../config/constants'

import {
    fetchAll,
    prepareFetchAllRequest,
} from "./../../../Flux/User/action/admin/fetchAll";

import {
    update,
} from "./../../../Flux/User/action/admin/update";

import {RequestStatus} from './../../../config/constants'

const mapState = ({
    user: {
        users,
        fetchRequestStatus: fetchUserDataRequestStatus,
        changeRequestStatus: changeUserRequestStatus,
    }
}) => {
    return {
        users,
        fetchUserDataRequestStatus,
        changeUserRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    update,
    fetchAll,
    prepareFetchAllRequest,
}, dispatch);

class ListHandler extends React.Component {
    componentDidMount() {
        const {
            users,
            fetchAll,
            prepareFetchAllRequest,
            fetchUserDataRequestStatus,
        } = this.props;

        switch (fetchUserDataRequestStatus) {
            case RequestStatus.VIRGIN:
                users.isEmpty() && fetchAll();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchAllRequest()
        }
    }

    componentWillReceiveProps({
        users,
        fetchAll,
        fetchUserDataRequestStatus,
    }) {
        if (fetchUserDataRequestStatus === RequestStatus.VIRGIN && users.isEmpty()) {
            fetchAll();
        }
    }

    render() {
        const {
            update,
            users,
            fetchUserDataRequestStatus,
            changeUserRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 24}}>
                    <Card title={`List of Users`}>
                        <UsersTable {...{
                            update,
                            users,
                            loading: fetchUserDataRequestStatus === RequestStatus.STARTED,
                            changeUserRequestStatus: changeUserRequestStatus === RequestStatus.STARTED,
                        }} />
                        <Button type="primary" onClick={ev => browserHistory.push(route.ADMIN.USER.CREATE)}>Create</Button>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(ListHandler);