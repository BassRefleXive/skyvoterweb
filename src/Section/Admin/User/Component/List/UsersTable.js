import React from 'react';
import {Table, Button} from 'antd';
import constants from './../../../../../Flux/User/constants'
import {composeUrl} from './../../../../../utils/route'
import {route} from './../../../../../config/constants'
import ButtonLink from './../../../../../Component/Navigation/ButtonLink'

export default class UsersTable extends React.Component {
    state = {
        changingId: undefined,
    };

    columns() {
        return [
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: 'Type',
                dataIndex: 'type',
                key: 'type',
            }, {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    return (
                        <Button.Group>
                            <Button {...{
                                loading: this.props.changeUserRequestStatus && this.state.changingId === record.id,
                                type: 'primary',
                                onClick: () => this.setState({changingId: record.id}) || this.props.update(
                                    record.id,
                                    {status: record.isActive() ? constants.USER_STATUS_BANNED : constants.USER_STATUS_ACTIVE}
                                )
                            }}>Toggle Status</Button>
                            <ButtonLink {...{
                                to: composeUrl(route.ADMIN.SERVER.LIST, {}, {owner: record.id}),
                                text: 'Servers',
                            }} />
                        </Button.Group>
                    );
                },
            }
        ];
    }

    render() {
        const {
            users,
            loading,
        } = this.props;

        return (
            <Table {...{
                columns: this.columns(),
                dataSource: users.toArray(),
                rowKey: record => record.id,
                loading,
            }} />
        );
    }
}