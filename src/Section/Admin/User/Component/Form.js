import React from 'react';
import {Form as AntForm, Input, Select, Button, Row, Col} from 'antd';
import constants from "../../../../Flux/User/constants";

class Form extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const {
            goBack,
            loading,
            form: {
                getFieldDecorator,
            }
        } = this.props;

        return (
            <AntForm onSubmit={this.handleSubmit}>

                <AntForm.Item>
                    {getFieldDecorator('email', {
                        rules: [{required: true, message: 'Please input email!'}],
                    })(
                        <Input {...{
                            placeholder: 'Email',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Please input password!'}],
                    })(
                        <Input {...{
                            placeholder: 'Password',
                            type: 'password',
                        }} />
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    {getFieldDecorator('type', {
                        initialValue: constants.USER_TYPE_CLIENT,
                        rules: [{required: true, message: 'Please select type!'}],
                    })(
                        <Select>
                            <Select.Option value={constants.USER_TYPE_CLIENT} key={constants.USER_TYPE_CLIENT}>Client</Select.Option>
                            <Select.Option value={constants.USER_TYPE_ADMIN} key={constants.USER_TYPE_ADMIN}>Admin</Select.Option>
                        </Select>
                    )}
                </AntForm.Item>

                <AntForm.Item>
                    <Button.Group>
                        <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
                        <Button onClick={goBack}>Back</Button>
                    </Button.Group>
                </AntForm.Item>
            </AntForm>
        );
    }
}

export default AntForm.create()(Form);