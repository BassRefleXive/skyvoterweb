import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Card, Row, Col} from 'antd';

import {RequestStatus} from './../../../config/constants'

import {
    create,
} from "./../../../Flux/User/action/admin/create";
import {browserHistory} from "react-router";
import {route} from "../../../config/constants";

import Form from './Component/Form'

const mapState = ({
                      user: {
                          createRequestStatus,
                      },
                  }, {router: {goBack}}) => {
    return {
        goBack,
        createRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    create,
}, dispatch);

class CreateHandler extends React.Component {

    componentWillReceiveProps(nextProps) {
        this.props.createRequestStatus === RequestStatus.STARTED &&
        nextProps.createRequestStatus !== RequestStatus.STARTED &&
        browserHistory.push(route.ADMIN.USER.LIST);
    }

    render() {
        const {
            create,
            goBack,
            createRequestStatus,
        } = this.props;

        return (
            <Row>
                <Col {...{span: 8, offset: 8}}>
                    <Card title={`Add New User`}>
                        <Form {...{
                            onSubmit: create,
                            goBack,
                            loading: createRequestStatus === RequestStatus.STARTED,
                        }}/>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(mapState, mapDispatch)(CreateHandler);