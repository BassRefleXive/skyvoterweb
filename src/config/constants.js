export const route = {
    ROUTE_LANDING: "/",
    ROUTE_CLIENTS: "/clients",
    ROUTE_TOPS: "/top",
    ROUTE_CLIENT: "/clients/:clientId",
    ROUTE_USER_LOGIN: "/login",

    ADMIN: {
        TOP: {
            LIST: '/admin/top',
            EDIT: '/admin/top/:id/edit',
            CREATE: '/admin/top/create',
        },
        CAPTCHA_RESOLVER: {
            LIST: '/admin/captcha-resolver',
            EDIT: '/admin/captcha-resolver/:id/edit',
            CREATE: '/admin/captcha-resolver/create',
        },
        USER: {
            LIST: '/admin/user',
            CREATE: '/admin/user/create',
        },
        SERVER: {
            LIST: '/admin/server',
            CREATE: '/admin/server/create',
        },
        JOB: {
            LIST: '/admin/job',
            EDIT: '/admin/job/:id/edit',
            CREATE: '/admin/job/create',
        },
    }
};

export const Http = {
    OK: 200,
    CREATED: 201,
    NO_CONTENT: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    REQUEST_TIMEOUT: 408,
    UNPROCESSABLE_ENTITY: 422,
    INTERNAL_SERVER_ERROR: 500,
};

export const RequestStatus = {
    VIRGIN: 0,
    FINISHED: 1,
    STARTED: 2,
};