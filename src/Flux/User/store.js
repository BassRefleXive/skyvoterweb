import {Map} from "immutable"

import {initialState} from "./init"
import userType from './constants'
import {RequestStatus} from './../../config/constants'

import User from './User'

const hydrateUserData = ({
    id,
    email,
    status,
    type,
}) => {
    return new User({
        id,
        email,
        status,
        type,
    })
};

const hydrateUsersData = data => {
    let users = Map();

    data.map(userData => {
        users = users.set(userData.id, hydrateUserData(userData))
    });

    return users;
};

const actionsMap = {
    [userType.USER_LOGIN_ERROR]: () => ({loginRequestStatus: RequestStatus.FINISHED}),
    [userType.USER_LOGIN_PREPARE]: () => ({loginRequestStatus: RequestStatus.VIRGIN}),
    [userType.USER_LOGIN_START]: () => ({loginRequestStatus: RequestStatus.STARTED}),
    [userType.USER_LOGIN_SUCCESS]: (store, {data}) => {
        return {
            token: data.token,
            loginRequestStatus: RequestStatus.FINISHED,
        }
    },

    [userType.USER_LOGOUT_SUCCESS]: () => ({user: new User()}),

    [userType.USER_ME_ERROR]: () => ({fetchUserDataRequestStatus: RequestStatus.FINISHED}),
    [userType.USER_ME_PREPARE]: () => ({fetchUserDataRequestStatus: RequestStatus.VIRGIN}),
    [userType.USER_ME_START]: () => ({fetchUserDataRequestStatus: RequestStatus.STARTED}),
    [userType.USER_ME_SUCCESS]: (store, {data}) => {
        return {
            user: hydrateUserData(data),
            fetchUserDataRequestStatus: RequestStatus.FINISHED,
        }
    },

    [userType.FETCH_USERS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [userType.FETCH_USERS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [userType.FETCH_USERS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [userType.FETCH_USERS_SUCCESS]: (store, {data}) => {
        return {
            users: hydrateUsersData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [userType.FETCH_USER_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [userType.FETCH_USER_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [userType.FETCH_USER_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [userType.FETCH_USER_SUCCESS]: (store, {data}) => {
        return {
            users: store.users.set(data.id, hydrateUserData(data)),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [userType.UPDATE_USER_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [userType.UPDATE_USER_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [userType.UPDATE_USER_SUCCESS]: (store, {data}) => {
        return {
            users: store.users.set(data.id, hydrateUserData(data)),
            changeRequestStatus: RequestStatus.FINISHED,
        }
    },

    [userType.CREATE_USER_ERROR]: () => ({createRequestStatus: RequestStatus.FINISHED}),
    [userType.CREATE_USER_START]: () => ({createRequestStatus: RequestStatus.STARTED}),
    [userType.CREATE_USER_SUCCESS]: (store, {data}) => {
        return {
            users: store.users.set(data.id, hydrateUserData(data)),
            createRequestStatus: RequestStatus.FINISHED,
        }
    },
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];
    if (!reduceFn) {
        return state
    }

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer