import {fetchHelper} from "./../../Action/utils"
import userType from './../constants'

export const fetchUserData = () =>
    fetchHelper({
        errorType: userType.USER_ME_ERROR,
        url: `/user/me`,
        before: {
            type: userType.USER_ME_START,
        },
        opts: {
            authenticated: true,
        },
        success: (data) => ({
            type: userType.USER_ME_SUCCESS,
            data,
        })
    });


export const prepareFetchUserDataRequest = () => ({
    type: userType.USER_ME_PREPARE,
});