import userType from './../constants'

export const logout = () => dispatch => {
    localStorage.removeItem('token');

    return dispatch({
        type: userType.USER_LOGOUT_SUCCESS
    });
};