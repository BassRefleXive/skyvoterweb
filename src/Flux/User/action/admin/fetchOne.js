import {fetchHelper} from "./../../../Action/utils"
import userType from './../../constants'

export const fetchOne = id =>
    fetchHelper({
        errorType: userType.FETCH_USER_ERROR,
        url: `/admin/user/${id}`,
        before: {
            type: userType.FETCH_USER_START,
        },
        opts: {
            authenticated: true,
        },
        success: data => ({
            type: userType.FETCH_USER_SUCCESS,
            data,
        })
    });


export const prepareFetchOneRequest = () => ({
    type: userType.FETCH_USER_PREPARE,
});