import {fetchHelper} from "./../../../Action/utils"
import userType from './../../constants'

export const update = (id, data) =>
    fetchHelper({
        errorType: userType.UPDATE_USER_ERROR,
        url: `/admin/user/${id}`,
        before: {
            type: userType.UPDATE_USER_START,
        },
        opts: {
            method: 'PUT',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: userType.UPDATE_USER_SUCCESS,
                data,
            };
        }
    });