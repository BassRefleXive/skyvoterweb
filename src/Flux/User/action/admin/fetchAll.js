import {fetchHelper} from "./../../../Action/utils"
import userType from './../../constants'

export const fetchAll = () =>
    fetchHelper({
        errorType: userType.FETCH_USERS_ERROR,
        url: `/admin/user/`,
        before: {
            type: userType.FETCH_USERS_START,
        },
        opts: {
            authenticated: true,
        },
        success: (data) => ({
            type: userType.FETCH_USERS_SUCCESS,
            data,
        })
    });


export const prepareFetchAllRequest = () => ({
    type: userType.FETCH_USERS_PREPARE,
});