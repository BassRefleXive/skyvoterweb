import {fetchHelper} from "./../../../Action/utils"
import userType from './../../constants'

export const create = data =>
    fetchHelper({
        errorType: userType.CREATE_USER_ERROR,
        url: `/admin/user/`,
        before: {
            type: userType.CREATE_USER_START,
        },
        opts: {
            method: 'POST',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: userType.CREATE_USER_SUCCESS,
                data,
            };
        }
    });