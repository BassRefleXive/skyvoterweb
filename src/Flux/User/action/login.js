import {fetchHelper} from "./../../Action/utils"
import userType from './../constants'

export const login = (email, password) =>
    fetchHelper({
        errorType: userType.USER_LOGIN_ERROR,
        url: `/login`,
        before: {
            type: userType.USER_LOGIN_START,
        },
        opts: {
            method: 'POST',
            body: {email, password},
        },
        success: data => {
            localStorage.setItem('token', data.token);

            return {
                type: userType.USER_LOGIN_SUCCESS,
                data,
            };
        },
    });


export const prepareLoginRequest = () => ({
    type: userType.USER_LOGIN_PREPARE,
});