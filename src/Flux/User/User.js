import {Record} from "immutable"
import constants from './constants'

export default class User extends Record({
    id: undefined,
    email: undefined,
    status: undefined,
    type: undefined,
}) {
    isInitialized() {
        return !!this.id;
    }

    isAdmin() {
        return this.isInitialized() && constants.USER_TYPE_ADMIN === this.type;
    }

    isClient() {
        return this.isInitialized() && constants.USER_TYPE_CLIENT === this.type;
    }

    isActive() {
        return this.isInitialized() && constants.USER_STATUS_ACTIVE === this.status;
    }

    isbanned() {
        return this.isInitialized() && constants.USER_STATUS_BANNED === this.status;
    }
}