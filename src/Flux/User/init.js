import {Map, Set} from "immutable"
import User from "./User"
import {initialData} from './../initialData'
import {RequestStatus} from './../../config/constants'

export const init = (initialData, data) => {
    return {
        user: new User(),
        users: Map(),
        loginRequestStatus: RequestStatus.VIRGIN,
        fetchUserDataRequestStatus: RequestStatus.VIRGIN,
        fetchRequestStatus: RequestStatus.VIRGIN,
        changeRequestStatus: RequestStatus.VIRGIN,
        createRequestStatus: RequestStatus.VIRGIN,
        removeRequestStatus: RequestStatus.VIRGIN,
    }
};

export const initialState = init(initialData);