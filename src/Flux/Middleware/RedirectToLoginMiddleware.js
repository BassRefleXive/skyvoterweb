import constants from './../User/constants'
import {browserHistory} from 'react-router'
import {route} from './../../config/constants'

const RedirectToLoginMiddleware = store => next => action => {
    if (action.type === constants.USER_ME_ERROR || action.status === 401) {
        browserHistory.push(route.ROUTE_USER_LOGIN);

        return next({type: constants.USER_LOGOUT_SUCCESS});
    }

    return next(action)
};

export default RedirectToLoginMiddleware;