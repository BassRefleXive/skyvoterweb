import {createStore, applyMiddleware, compose} from 'redux'
import ReduxThunk from 'redux-thunk'

import reducer from "./reducer"
import init from "Flux/initialState"

import RedirectToLoginMiddleware from './Middleware/RedirectToLoginMiddleware'

const store = createStore(
    reducer,
    init({}, {}),
    compose(applyMiddleware(ReduxThunk, RedirectToLoginMiddleware))
);

export default store