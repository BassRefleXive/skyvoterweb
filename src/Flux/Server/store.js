import {Map} from "immutable"

import {initialState} from "./init"
import serverType from './constants'
import {RequestStatus} from './../../config/constants'

import Server from './Server'

const hydrateServerData = ({
    id,
    title,
    provider_id: providerId,
    status,
    top: {
        id: topId
    },
    owner: {
        id: ownerId
    },
}) => {
    return new Server({
        id,
        title,
        providerId,
        status,
        topId,
        ownerId,
    })
};

const hydrateServersData = data => {
    let servers = Map();

    data.map(serverData => {
        servers = servers.set(serverData.id, hydrateServerData(serverData))
    });

    return servers;
};

const actionsMap = {
    [serverType.FETCH_SERVERS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [serverType.FETCH_SERVERS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [serverType.FETCH_SERVERS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [serverType.FETCH_SERVERS_SUCCESS]: (store, {data}) => {
        return {
            servers: hydrateServersData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [serverType.FETCH_SERVER_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [serverType.FETCH_SERVER_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [serverType.FETCH_SERVER_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [serverType.FETCH_SERVER_SUCCESS]: (store, {data}) => {
        return {
            servers: store.servers.set(data.id, hydrateServerData(data)),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [serverType.UPDATE_SERVER_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [serverType.UPDATE_SERVER_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [serverType.UPDATE_SERVER_SUCCESS]: (store, {data}) => {
        return {
            servers: store.servers.set(data.id, hydrateServerData(data)),
            changeRequestStatus: RequestStatus.FINISHED,
        }
    },

    [serverType.CREATE_SERVER_ERROR]: () => ({createRequestStatus: RequestStatus.FINISHED}),
    [serverType.CREATE_SERVER_START]: () => ({createRequestStatus: RequestStatus.STARTED}),
    [serverType.CREATE_SERVER_SUCCESS]: (store, {data}) => {
        return {
            servers: store.servers.set(data.id, hydrateServerData(data)),
            createRequestStatus: RequestStatus.FINISHED,
        }
    },

    [serverType.REMOVE_SERVER_ERROR]: () => ({removeRequestStatus: RequestStatus.FINISHED}),
    [serverType.REMOVE_SERVER_START]: () => ({removeRequestStatus: RequestStatus.STARTED}),
    [serverType.REMOVE_SERVER_SUCCESS]: (store, {data}) => {
        return {
            servers: store.servers.delete(data.id),
            removeRequestStatus: RequestStatus.FINISHED,
        }
    },
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];

    if (!reduceFn) {
        return state
    }

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer