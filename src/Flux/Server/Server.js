import {Record} from "immutable"
import constants from "./constants";

export default class Server extends Record({
    id: undefined,
    title: undefined,
    providerId: undefined,
    status: undefined,
    topId: undefined,
    ownerId: undefined,
}) {
    isEnabled() {
        return constants.SERVER_STATUS_ENABLED === this.status;
    }

    isDisabled() {
        return constants.SERVER_STATUS_DISABLED === this.status;
    }

    isPaused() {
        return constants.SERVER_STATUS_PAUSED === this.status;
    }

    nextStatus() {
        if (this.isEnabled()) { return constants.SERVER_STATUS_PAUSED}
        if (this.isPaused()) { return constants.SERVER_STATUS_DISABLED}
        if (this.isDisabled()) { return constants.SERVER_STATUS_ENABLED}
    }
}