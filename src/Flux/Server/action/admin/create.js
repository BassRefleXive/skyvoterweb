import {fetchHelper} from "./../../../Action/utils"
import serverType from './../../constants'

export const create = data =>
    fetchHelper({
        errorType: serverType.CREATE_SERVER_ERROR,
        url: `/admin/server/`,
        before: {
            type: serverType.CREATE_SERVER_START,
        },
        opts: {
            method: 'POST',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: serverType.CREATE_SERVER_SUCCESS,
                data,
            };
        }
    });