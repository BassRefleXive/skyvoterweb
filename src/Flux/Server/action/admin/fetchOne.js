import {fetchHelper} from "./../../../Action/utils"
import serverType from './../../constants'

export const fetchOne = id =>
    fetchHelper({
        errorType: serverType.FETCH_SERVER_ERROR,
        url: `/admin/server/${id}`,
        before: {
            type: serverType.FETCH_SERVER_START,
        },
        opts: {
            authenticated: true,
        },
        success: data => ({
            type: serverType.FETCH_SERVER_SUCCESS,
            data,
        })
    });


export const prepareFetchOneRequest = () => ({
    type: serverType.FETCH_SERVER_PREPARE,
});