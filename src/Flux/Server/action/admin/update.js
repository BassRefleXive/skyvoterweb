import {fetchHelper} from "./../../../Action/utils"
import serverType from './../../constants'

export const update = (id, data) =>
    fetchHelper({
        errorType: serverType.UPDATE_SERVER_ERROR,
        url: `/admin/server/${id}`,
        before: {
            type: serverType.UPDATE_SERVER_START,
        },
        opts: {
            method: 'PUT',
            body: data,
            authenticated: true,
        },
        success: data => ({
            type: serverType.UPDATE_SERVER_SUCCESS,
            data,
        })
    });