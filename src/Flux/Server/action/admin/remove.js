import {fetchHelper} from "./../../../Action/utils"
import serverType from './../../constants'

export const remove = id =>
    fetchHelper({
        errorType: serverType.REMOVE_SERVER_ERROR,
        url: `/admin/server/${id}`,
        before: {
            type: serverType.REMOVE_SERVER_START,
        },
        opts: {
            method: 'DELETE',
            authenticated: true,
        },
        success: () => ({
            type: serverType.REMOVE_SERVER_SUCCESS,
            data: {id},
        })
    });