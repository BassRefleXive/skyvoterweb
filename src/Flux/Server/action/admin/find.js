import {fetchHelper} from "./../../../Action/utils"
import serverType from './../../constants'
import {stringify} from 'query-string'

export const find = (ids, ownerId) => fetchHelper({
    errorType: serverType.FETCH_SERVERS_ERROR,
    url: `/admin/server/find?${stringify({ids: ids, owner_id: ownerId}, {arrayFormat: 'bracket'})}`,
    before: {
        type: serverType.FETCH_SERVERS_START,
    },
    opts: {
        authenticated: true,
    },
    success: data => ({
        type: serverType.FETCH_SERVERS_SUCCESS,
        data,
    })
});

export const prepareFindRequest = () => ({
    type: serverType.FETCH_SERVERS_PREPARE,
});