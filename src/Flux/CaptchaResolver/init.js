import {Map, Set} from "immutable"
import {initialData} from './../initialData'
import {RequestStatus} from './../../config/constants'

export const init = (initialData, data) => {
    return {
        resolvers: Map(),
        fetchRequestStatus: RequestStatus.VIRGIN,
        changeRequestStatus: RequestStatus.VIRGIN,
        createRequestStatus: RequestStatus.VIRGIN,
        removeRequestStatus: RequestStatus.VIRGIN,
    }
};

export const initialState = init(initialData);