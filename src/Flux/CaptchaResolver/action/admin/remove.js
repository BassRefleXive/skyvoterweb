import {fetchHelper} from "./../../../Action/utils"
import captchaResolverType from './../../constants'

export const remove = id =>
    fetchHelper({
        errorType: captchaResolverType.REMOVE_CAPTCHA_RESOLVER_ERROR,
        url: `/admin/captcha-resolver/${id}`,
        before: {
            type: captchaResolverType.REMOVE_CAPTCHA_RESOLVER_START,
        },
        opts: {
            method: 'DELETE',
            authenticated: true,
        },
        success: () => {
            return {
                type: captchaResolverType.REMOVE_CAPTCHA_RESOLVER_SUCCESS,
                data: {id},
            };
        }
    });