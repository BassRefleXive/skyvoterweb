import {fetchHelper} from "./../../../Action/utils"
import captchaResolverType from './../../constants'

export const update = (id, data) =>
    fetchHelper({
        errorType: captchaResolverType.UPDATE_CAPTCHA_RESOLVER_ERROR,
        url: `/admin/captcha-resolver/${id}`,
        before: {
            type: captchaResolverType.UPDATE_CAPTCHA_RESOLVER_START,
        },
        opts: {
            method: 'PUT',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: captchaResolverType.UPDATE_CAPTCHA_RESOLVER_SUCCESS,
                data,
            };
        }
    });