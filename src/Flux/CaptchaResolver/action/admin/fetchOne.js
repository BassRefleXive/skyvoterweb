import {fetchHelper} from "./../../../Action/utils"
import captchaResolverType from './../../constants'

export const fetchOne = id =>
    fetchHelper({
        errorType: captchaResolverType.FETCH_CAPTCHA_RESOLVER_ERROR,
        url: `/admin/captcha-resolver/${id}`,
        before: {
            type: captchaResolverType.FETCH_CAPTCHA_RESOLVER_START,
        },
        opts: {
            authenticated: true,
        },
        success: data => ({
            type: captchaResolverType.FETCH_CAPTCHA_RESOLVER_SUCCESS,
            data,
        })
    });


export const prepareFetchOneRequest = () => ({
    type: captchaResolverType.FETCH_CAPTCHA_RESOLVER_PREPARE,
});