import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const fetchAll = () =>
    fetchHelper({
        errorType: topType.FETCH_CAPTCHA_RESOLVERS_ERROR,
        url: `/admin/captcha-resolver/`,
        before: {
            type: topType.FETCH_CAPTCHA_RESOLVERS_START,
        },
        opts: {
            authenticated: true,
        },
        success: (data) => ({
            type: topType.FETCH_CAPTCHA_RESOLVERS_SUCCESS,
            data,
        })
    });


export const prepareFetchAllRequest = () => ({
    type: topType.FETCH_CAPTCHA_RESOLVERS_PREPARE,
});