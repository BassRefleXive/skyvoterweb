import {fetchHelper} from "./../../../Action/utils"
import captchaResolverType from './../../constants'

export const create = data =>
    fetchHelper({
        errorType: captchaResolverType.CREATE_CAPTCHA_RESOLVER_ERROR,
        url: `/admin/captcha-resolver/`,
        before: {
            type: captchaResolverType.CREATE_CAPTCHA_RESOLVER_START,
        },
        opts: {
            method: 'POST',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: captchaResolverType.CREATE_CAPTCHA_RESOLVER_SUCCESS,
                data,
            };
        }
    });