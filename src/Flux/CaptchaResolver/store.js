import {Map} from "immutable"

import {initialState} from "./init"
import captchaResolverType from './constants'
import {RequestStatus} from './../../config/constants'

import CaptchaResolver from './CaptchaResolver'

const hydrateCaptchaResolverData = ({
    id,
    code,
    key,
}) => {
    return new CaptchaResolver({
        id,
        code,
        key,
    })
};

const hydrateCaptchaResolversData = data => {
    let resolvers = Map();

    data.map(resolverData => {
        resolvers = resolvers.set(resolverData.id, hydrateCaptchaResolverData(resolverData))
    });

    return resolvers;
};

const actionsMap = {
    [captchaResolverType.FETCH_CAPTCHA_RESOLVERS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVERS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVERS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVERS_SUCCESS]: (store, {data}) => {
        return {
            resolvers: hydrateCaptchaResolversData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [captchaResolverType.FETCH_CAPTCHA_RESOLVER_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVER_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVER_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [captchaResolverType.FETCH_CAPTCHA_RESOLVER_SUCCESS]: (store, {data}) => {
        return {
            resolvers: store.resolvers.set(data.id, hydrateCaptchaResolverData(data)),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [captchaResolverType.UPDATE_CAPTCHA_RESOLVER_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [captchaResolverType.UPDATE_CAPTCHA_RESOLVER_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [captchaResolverType.UPDATE_CAPTCHA_RESOLVER_SUCCESS]: (store, {data}) => {
        return {
            resolvers: store.resolvers.set(data.id, hydrateCaptchaResolverData(data)),
            changeRequestStatus: RequestStatus.FINISHED,
        }
    },

    [captchaResolverType.CREATE_CAPTCHA_RESOLVER_ERROR]: () => ({createRequestStatus: RequestStatus.FINISHED}),
    [captchaResolverType.CREATE_CAPTCHA_RESOLVER_START]: () => ({createRequestStatus: RequestStatus.STARTED}),
    [captchaResolverType.CREATE_CAPTCHA_RESOLVER_SUCCESS]: (store, {data}) => {
        return {
            resolvers: store.resolvers.set(data.id, hydrateCaptchaResolverData(data)),
            createRequestStatus: RequestStatus.FINISHED,
        }
    },

    [captchaResolverType.REMOVE_CAPTCHA_RESOLVER_ERROR]: () => ({removeRequestStatus: RequestStatus.FINISHED}),
    [captchaResolverType.REMOVE_CAPTCHA_RESOLVER_START]: () => ({removeRequestStatus: RequestStatus.STARTED}),
    [captchaResolverType.REMOVE_CAPTCHA_RESOLVER_SUCCESS]: (store, {data}) => {
        return {
            resolvers: store.resolvers.delete(data.id),
            removeRequestStatus: RequestStatus.FINISHED,
        }
    },
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];

    if (!reduceFn) {
        return state
    }
    console.log('reducer', action);

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer