import {Record} from "immutable"
import constants from "./constants";

export default class CaptchaResolver extends Record({
    id: undefined,
    code: undefined,
    key: undefined,
}) {
}