import {combineReducers} from 'redux'
import {routerReducer as routing} from 'react-router-redux'

import tops from './Top/store'
import clients from './Clients/store'
import user from './User/store'
import captchaResolver from './CaptchaResolver/store'
import server from './Server/store'
import job from './Job/store'

export default combineReducers({
    routing,
    tops,
    clients,
    user,
    captchaResolver,
    server,
    job,
})