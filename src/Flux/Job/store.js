import {Map} from "immutable"

import {initialState} from "./init"
import jobType from './constants'
import {RequestStatus} from './../../config/constants'

import Job from './Job'

const hydrateJobData = ({
    id,
    date,
    status,
    ordered_votes: orderedVotes,
    pending_votes: pendingVotes,
    executed_votes: executedVotes,
    failed_votes: failedVotes,
    server: {id: serverId},
}) => {
    return new Job({
        id,
        date,
        status,
        orderedVotes,
        pendingVotes,
        executedVotes,
        failedVotes,
        serverId,
    })
};

const hydrateJobsData = data => {
    let jobs = Map();

    data.map(jobData => {
        jobs = jobs.set(jobData.id, hydrateJobData(jobData))
    });

    return jobs;
};

const actionsMap = {
    [jobType.FETCH_JOBS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [jobType.FETCH_JOBS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [jobType.FETCH_JOBS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [jobType.FETCH_JOBS_SUCCESS]: (store, {data}) => {
        return {
            jobs: hydrateJobsData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [jobType.FETCH_JOB_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [jobType.FETCH_JOB_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [jobType.FETCH_JOB_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [jobType.FETCH_JOB_SUCCESS]: (store, {data}) => {
        return {
            jobs: store.jobs.set(data.id, hydrateJobData(data)),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [jobType.UPDATE_JOB_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [jobType.UPDATE_JOB_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [jobType.UPDATE_JOB_SUCCESS]: (store, {data}) => {
        return {
            jobs: store.jobs.set(data.id, hydrateJobData(data)),
            changeRequestStatus: RequestStatus.FINISHED,
        }
    },

    [jobType.CREATE_JOB_ERROR]: () => ({createRequestStatus: RequestStatus.FINISHED}),
    [jobType.CREATE_JOB_START]: () => ({createRequestStatus: RequestStatus.STARTED}),
    [jobType.CREATE_JOB_SUCCESS]: (store, {data}) => {
        return {
            jobs: hydrateJobsData(data),
            createRequestStatus: RequestStatus.FINISHED,
        }
    },

    [jobType.REMOVE_JOB_ERROR]: () => ({removeRequestStatus: RequestStatus.FINISHED}),
    [jobType.REMOVE_JOB_START]: () => ({removeRequestStatus: RequestStatus.STARTED}),
    [jobType.REMOVE_JOB_SUCCESS]: (store, {data}) => {
        return {
            jobs: store.jobs.delete(data.id),
            removeRequestStatus: RequestStatus.FINISHED,
        }
    },
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];

    if (!reduceFn) {
        return state
    }

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer