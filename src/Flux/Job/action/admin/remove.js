import {fetchHelper} from "./../../../Action/utils"
import jobType from './../../constants'

export const remove = id =>
    fetchHelper({
        errorType: jobType.REMOVE_JOB_ERROR,
        url: `/admin/job/${id}`,
        before: {
            type: jobType.REMOVE_JOB_START,
        },
        opts: {
            method: 'DELETE',
            authenticated: true,
        },
        success: () => ({
            type: jobType.REMOVE_JOB_SUCCESS,
            data: {id},
        })
    });