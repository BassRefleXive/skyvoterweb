import {fetchHelper} from "./../../../Action/utils"
import jobType from './../../constants'

export const update = (id, data) =>
    fetchHelper({
        errorType: jobType.UPDATE_JOB_ERROR,
        url: `/admin/job/${id}`,
        before: {
            type: jobType.UPDATE_JOB_START,
        },
        opts: {
            method: 'PUT',
            body: data,
            authenticated: true,
        },
        success: data => {
            return {
                type: jobType.UPDATE_JOB_SUCCESS,
                data,
            };
        }
    });