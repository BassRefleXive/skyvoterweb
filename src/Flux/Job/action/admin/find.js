import {fetchHelper} from "./../../../Action/utils"
import jobType from './../../constants'
import {stringify} from 'query-string'

export const find = (serverId, startDate, endDate, status) => fetchHelper({
    errorType: jobType.FETCH_JOBS_ERROR,
    url: `/admin/job/find?${stringify({
        server_id: serverId,
        start_date: startDate,
        end_date: endDate,
        status
    })}`,
    before: {
        type: jobType.FETCH_JOBS_START,
    },
    opts: {
        authenticated: true,
    },
    success: data => ({
        type: jobType.FETCH_JOBS_SUCCESS,
        data,
    })
});

export const prepareFindRequest = () => ({
    type: jobType.FETCH_JOBS_PREPARE,
});