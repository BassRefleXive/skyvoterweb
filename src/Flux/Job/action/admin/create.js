import {fetchHelper} from "./../../../Action/utils"
import jobType from './../../constants'

export const create = (serverId, data) =>
    fetchHelper({
        errorType: jobType.CREATE_JOB_ERROR,
        url: `/admin/job/`,
        before: {
            type: jobType.CREATE_JOB_START,
        },
        opts: {
            method: 'POST',
            body: {
                server_id: serverId,
                jobs: data,
            },
            authenticated: true,
        },
        success: data => {
            return {
                type: jobType.CREATE_JOB_SUCCESS,
                data,
            };
        }
    });