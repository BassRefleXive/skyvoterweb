import {fetchHelper} from "./../../../Action/utils"
import jobType from './../../constants'

export const fetchOne = id =>
    fetchHelper({
        errorType: jobType.FETCH_JOB_ERROR,
        url: `/admin/job/${id}`,
        before: {
            type: jobType.FETCH_JOB_START,
        },
        opts: {
            authenticated: true,
        },
        success: data => ({
            type: jobType.FETCH_JOB_SUCCESS,
            data,
        })
    });


export const prepareFetchOneRequest = () => ({
    type: jobType.FETCH_JOB_PREPARE,
});