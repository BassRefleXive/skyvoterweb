import {Record} from "immutable"
import constants from "../Job/constants";

export default class Job extends Record({
    id: undefined,
    date: undefined,
    status: undefined,
    orderedVotes: undefined,
    pendingVotes: undefined,
    executedVotes: undefined,
    failedVotes: undefined,
    serverId: undefined,
}) {
    isEnabled() {
        return constants.JOB_STATUS_ENABLED === this.status;
    }

    isDisabled() {
        return constants.JOB_STATUS_DISABLED === this.status;
    }

    isPaused() {
        return constants.JOB_STATUS_PAUSED === this.status;
    }

    nextStatus() {
        if (this.isEnabled()) { return constants.JOB_STATUS_PAUSED}
        if (this.isPaused()) { return constants.JOB_STATUS_DISABLED}
        if (this.isDisabled()) { return constants.JOB_STATUS_ENABLED}
    }
}