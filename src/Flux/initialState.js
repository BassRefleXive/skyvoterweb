import {init as initTops} from './Top/init'
import {init as initUser} from './User/init'

export default function (initialData, data) {
    return {
        tops: initTops(initialData, data),
        user: initUser(initialData, data),
    }
}