import {Map} from "immutable"

import {initialState} from "./init"
import topType from './constants'
import {RequestStatus} from './../../config/constants'

import Top from './Top'

const hydrateTopData = ({
    id,
    title,
    code,
    multiplier,
    status,
    captcha_resolver: {
        id: captchaResolverId
    },
}) => {
    return new Top({
        id,
        title,
        code,
        multiplier,
        captchaResolverId,
        status,
    })
};

const hydrateTopsData = data => {
    let tops = Map();

    data.map(topData => {
        tops = tops.set(topData.id, hydrateTopData(topData))
    });

    return tops;
};

const actionsMap = {
    [topType.FETCH_TOPS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [topType.FETCH_TOPS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [topType.FETCH_TOPS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [topType.FETCH_TOPS_SUCCESS]: (store, {data}) => {
        return {
            tops: hydrateTopsData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [topType.FETCH_TOP_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [topType.FETCH_TOP_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [topType.FETCH_TOP_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [topType.FETCH_TOP_SUCCESS]: (store, {data}) => {
        return {
            tops: store.tops.set(data.id, hydrateTopData(data)),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [topType.UPDATE_TOP_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [topType.UPDATE_TOP_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [topType.UPDATE_TOP_SUCCESS]: (store, {data}) => {
        return {
            tops: store.tops.set(data.id, hydrateTopData(data)),
            changeRequestStatus: RequestStatus.FINISHED,
        }
    },

    [topType.CREATE_TOP_ERROR]: () => ({createRequestStatus: RequestStatus.FINISHED}),
    [topType.CREATE_TOP_START]: () => ({createRequestStatus: RequestStatus.STARTED}),
    [topType.CREATE_TOP_SUCCESS]: (store, {data}) => {
        return {
            tops: store.tops.set(data.id, hydrateTopData(data)),
            createRequestStatus: RequestStatus.FINISHED,
        }
    },

    [topType.REMOVE_TOP_ERROR]: () => ({removeRequestStatus: RequestStatus.FINISHED}),
    [topType.REMOVE_TOP_START]: () => ({removeRequestStatus: RequestStatus.STARTED}),
    [topType.REMOVE_TOP_SUCCESS]: (store, {data}) => {
        return {
            tops: store.tops.delete(data.id),
            removeRequestStatus: RequestStatus.FINISHED,
        }
    },
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];

    if (!reduceFn) {
        return state
    }
    console.log('reducer', action)

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer