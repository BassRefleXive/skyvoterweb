import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const fetchTopsData = () =>
    fetchHelper({
        errorType: topType.FETCH_TOPS_ERROR,
        url: `/admin/top/`,
        before: {
            type: topType.FETCH_TOPS_START,
        },
        opts: {
            authenticated: true,
        },
        success: (data) => ({
            type: topType.FETCH_TOPS_SUCCESS,
            data,
        })
    });


export const prepareFetchTopsDataRequest = () => ({
    type: topType.FETCH_TOPS_PREPARE,
});