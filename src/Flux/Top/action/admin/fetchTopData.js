import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const fetchTopData = topId =>
    fetchHelper({
        errorType: topType.FETCH_TOP_ERROR,
        url: `/admin/top/${topId}`,
        before: {
            type: topType.FETCH_TOP_START,
        },
        opts: {
            authenticated: true,
        },
        success: (data) => ({
            type: topType.FETCH_TOP_SUCCESS,
            data,
        })
    });


export const prepareFetchTopDataRequest = () => ({
    type: topType.FETCH_TOP_PREPARE,
});