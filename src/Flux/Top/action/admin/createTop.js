import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const createTop = (data) =>
    fetchHelper({
        errorType: topType.CREATE_TOP_ERROR,
        url: `/admin/top/`,
        before: {
            type: topType.CREATE_TOP_START,
        },
        opts: {
            method: 'POST',
            authenticated: true,
            body: data,
        },
        success: (data) => {
            return {
                type: topType.CREATE_TOP_SUCCESS,
                data,
            };
        }
    });