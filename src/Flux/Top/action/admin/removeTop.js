import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const removeTop = id =>
    fetchHelper({
        errorType: topType.REMOVE_TOP_ERROR,
        url: `/admin/top/${id}`,
        before: {
            type: topType.REMOVE_TOP_START,
        },
        opts: {
            method: 'DELETE',
            authenticated: true,
        },
        success: () => {
            return {
                type: topType.REMOVE_TOP_SUCCESS,
                data: {id},
            };
        }
    });