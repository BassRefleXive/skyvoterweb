import {fetchHelper} from "./../../../Action/utils"
import topType from './../../constants'

export const updateTop = (id, data) =>
    fetchHelper({
        errorType: topType.UPDATE_TOP_ERROR,
        url: `/admin/top/${id}`,
        before: {
            type: topType.UPDATE_TOP_START,
        },
        opts: {
            method: 'PUT',
            authenticated: true,
            body: data,
        },
        success: (data) => {
            return {
                type: topType.UPDATE_TOP_SUCCESS,
                data,
            };
        }
    });