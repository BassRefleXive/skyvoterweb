import {Record} from "immutable"
import constants from "./constants";

export default class Top extends Record({
    id: undefined,
    title: undefined,
    code: undefined,
    multiplier: undefined,
    captchaResolverId: undefined,
    status: undefined,
}) {
    isEnabled() {
        return constants.TOP_STATUS_ENABLED === this.status;
    }

    isDisabled() {
        return constants.TOP_STATUS_DISABLED === this.status;
    }

    isPaused() {
        return constants.TOP_STATUS_PAUSED === this.status;
    }
}