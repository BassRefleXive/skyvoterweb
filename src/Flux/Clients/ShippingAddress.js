import {Record} from "immutable"

export default class ShippingAddress extends Record({
    id: undefined,
    country: undefined,
    city: undefined,
    zipCode: undefined,
    street: undefined,
    default: undefined,
}) {

}