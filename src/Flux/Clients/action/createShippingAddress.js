import {fetchHelper} from "./../../Action/utils"
import clientsType from './../constants'

export const createShippingAddress = (clientId, data) =>
    fetchHelper({
        errorType: clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_ERROR,
        url: `/clients/${clientId}/shipping-addresses`,
        before: {
            type: clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_START,
        },
        opts: {
            method: 'POST',
            body: JSON.stringify(data),
        },
        success: (data) => ({
            type: clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_SUCCESS,
            data,
        })
    });

export const prepareCreateShippingAddress = clientId => ({
    type: clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_PREPARE,
    clientId,
});